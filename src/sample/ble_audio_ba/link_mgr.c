/**
*****************************************************************************************
*     Copyright(c) 2018, Realtek Semiconductor Corporation. All rights reserved.
*****************************************************************************************
   * @file      link_mgr.c
   * @brief     Link manager functions.
   * @author    berni
   * @date      2018-04-27
   * @version   v1.0
   **************************************************************************************
   * @attention
   * <h2><center>&copy; COPYRIGHT 2018 Realtek Semiconductor Corporation</center></h2>
   **************************************************************************************
  */
/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include <string.h>
#include "link_mgr.h"

/*============================================================================*
 *                              Variables
 *============================================================================*/
/** @} */
/** @addtogroup  BT5_CENTRAL_SCAN_MGR
    * @{
    */
T_DEV_INFO dev_list[APP_MAX_DEVICE_INFO];
uint8_t dev_list_count = 0;
T_APP_DB app_db;
/** @} */

/*============================================================================*
 *                              Functions
 *============================================================================*/
/** @addtogroup  BT5_CENTRAL_SCAN_MGR
    * @{
    */
/**
 * @brief   Add device information to device list.
 *
 * @param[in] bd_addr Peer device address.
 * @param[in] bd_type Peer device address type.
 * @retval true Success.
 * @retval false Failed, device list is full.
 */
bool link_mgr_add_device(uint8_t *bd_addr, uint8_t bd_type, T_DEV_INFO **pp_dev)
{
    *pp_dev = NULL;
    /* If result count not at max */
    if (dev_list_count < APP_MAX_DEVICE_INFO)
    {
        uint8_t i;
        /* Check if device is already in device list*/
        for (i = 0; i < dev_list_count; i++)
        {
            if (memcmp(bd_addr, dev_list[i].bd_addr, GAP_BD_ADDR_LEN) == 0)
            {
                return true;
            }
        }
        memset(&dev_list[dev_list_count], 0, sizeof(T_DEV_INFO));
        /*Add addr to device list list*/
        memcpy(dev_list[dev_list_count].bd_addr, bd_addr, GAP_BD_ADDR_LEN);
        dev_list[dev_list_count].bd_type = bd_type;
        dev_list[dev_list_count].idx = dev_list_count;
        *pp_dev = &dev_list[dev_list_count];
        /*Increment device list count*/
        dev_list_count++;
    }
    else
    {
        return false;
    }
    return true;
}

/**
 * @brief Clear device list.
 * @retval void
 */
void link_mgr_clear_device_list(void)
{
    dev_list_count = 0;
}

T_BLE_LINK *ble_link_find_by_conn_id(uint8_t conn_id)
{
    T_BLE_LINK *p_link = NULL;
    uint8_t        i;

    for (i = 0; i < APP_MAX_LINKS; i++)
    {
        if (app_db.le_link[i].used == true &&
            app_db.le_link[i].conn_id == conn_id)
        {
            p_link = &app_db.le_link[i];
            break;
        }
    }

    return p_link;
}

T_BLE_LINK *ble_link_find_by_conn_handle(uint8_t conn_handle)
{
    T_BLE_LINK *p_link = NULL;
    uint8_t        i;

    for (i = 0; i < APP_MAX_LINKS; i++)
    {
        if (app_db.le_link[i].used == true &&
            app_db.le_link[i].conn_handle == conn_handle)
        {
            p_link = &app_db.le_link[i];
            break;
        }
    }

    return p_link;
}

T_BLE_LINK *ble_link_find_by_addr(uint8_t *bd_addr, uint8_t bd_type)
{
    T_BLE_LINK *p_link = NULL;
    uint8_t        i;

    for (i = 0; i < APP_MAX_LINKS; i++)
    {
        if (app_db.le_link[i].used == true &&
            app_db.le_link[i].remote_bd_type == bd_type
            && memcmp(app_db.le_link[i].remote_bd, bd_addr, GAP_BD_ADDR_LEN) == 0)
        {
            p_link = &app_db.le_link[i];
            break;
        }
    }

    return p_link;
}

T_BLE_LINK *ble_link_alloc_by_conn_id(uint8_t conn_id)
{
    T_BLE_LINK *p_link = NULL;
    uint8_t        i;

    p_link = ble_link_find_by_conn_id(conn_id);
    if (p_link != NULL)
    {
        return p_link;
    }

    for (i = 0; i < APP_MAX_LINKS; i++)
    {
        if (app_db.le_link[i].used == false)
        {
            p_link = &app_db.le_link[i];

            p_link->used    = true;
            p_link->conn_id = conn_id;
            break;
        }
    }

    return p_link;
}

bool ble_link_free(T_BLE_LINK *p_link)
{
    if (p_link != NULL)
    {
        if (p_link->used == true)
        {
            memset(p_link, 0, sizeof(T_BLE_LINK));
            p_link->conn_id = 0xFF;
            return true;
        }
    }

    return false;
}
/** @} */
