/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     key_handle.c
* @brief    This is the entry of user code which the key handle module resides in.
* @details
* @author   chenjie jin
* @date     2020-02-25
* @version  v1.1
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include <string.h>
#include "board.h"
#include "key_handle.h"
#include <trace.h>
#include "profile_server.h"
#include "hids_rmc.h"
#include "rcu_application.h"
#include "os_timer.h"
#include "swtimer.h"
#include "voice.h"
#include "voice_driver.h"
#include "rcu_gap.h"
#include "gap_storage_le.h"
#include "rtl876x_wdg.h"
#include "rtl876x_keyscan.h"
#include "gap_bond_le.h"
#include "battery_driver.h"
#include "led_driver.h"
#include <app_section.h>
#if SUPPORT_IR_TX_FEATURE
#include "ir_send_handle.h"
#include "ir_service_handle.h"
#endif
#if FEATURE_SUPPORT_MP_TEST_MODE
#include "mp_test.h"
#endif
#include "atvv_service.h"
#include "app_custom.h"
#if SUPPORT_BUZZER_FEATURE
#include "buzzer_driver.h"
#include "fms_service_handle.h"
#endif

/*============================================================================*
 *                              Local Variables
 *============================================================================*/
#define MAX_KEY_PRESS_CACHE_FIFO_CNT    22

static uint8_t Key_Press_Cache_FIFO[MAX_KEY_PRESS_CACHE_FIFO_CNT];
static uint8_t current_cache_fifo_items_cnt = 0;
static uint8_t cache_fifo_output_cnt = 0;

/*============================================================================*
 *                              Global Variables
 *============================================================================*/
/* Key Mapping Table Definition, need to align with hardware key matrix */
const T_KEY_INDEX_DEF KEY_MAPPING_TABLE_G10[KEYPAD_MAX_ROW_SIZE_G10][KEYPAD_MAX_COLUMN_SIZE_G10] =
{
    {KEY_ID_Power,      KEY_ID_Input,     KEY_ID_Bookmark,    KEY_ID_Assistant, KEY_ID_Settings, KEY_ID_APP03, KEY_ID_NONE},
    {KEY_ID_DPadCenter, KEY_ID_DPadUp,    KEY_ID_DPadDown,    KEY_ID_DPadLeft,  KEY_ID_DPadRight, KEY_ID_APP04, KEY_ID_NONE},
    {KEY_ID_Back,       KEY_ID_Home,      KEY_ID_Guide,       KEY_ID_VolDown,   KEY_ID_VolDown, KEY_ID_NONE, KEY_ID_NONE},
    {KEY_ID_Mute,       KEY_ID_ChannelUp, KEY_ID_ChannelDown, KEY_ID_Youtube,   KEY_ID_Netflix, KEY_ID_NONE, KEY_ID_NONE},
};
/* settings, guide, bookmark index in KEY_MAPPING_TABLE_G10 */
const uint8_t Kb_Map_devicelayout_Correspend_Index_G10[] =  {4, 16, 2};
const uint8_t Wakeup_Map_Correspend_Index_G10[] =
{
    KEY_ID_Assistant, KEY_ID_Home, KEY_ID_Power, KEY_ID_Input, KEY_ID_Youtube, KEY_ID_Netflix, KEY_ID_APP03, KEY_ID_APP04
};
uint8_t Ir_Override_Table_Index_G10[24] =
{
    KEY_ID_Power,       KEY_ID_DPadRight,   KEY_ID_VolDown,     KEY_ID_Input,       KEY_ID_DPadDown,    KEY_ID_ChannelDown,
    KEY_ID_Bookmark,    KEY_ID_Back,        KEY_ID_Youtube,     KEY_ID_Assistant,   KEY_ID_Home,        KEY_ID_Netflix,
    KEY_ID_Settings,    KEY_ID_Guide,       KEY_ID_APP03,       KEY_ID_DPadUp,      KEY_ID_VolUp,       KEY_ID_APP04,
    KEY_ID_DPadLeft,    KEY_ID_ChannelUp,   KEY_ID_NONE,        KEY_ID_DPadCenter,  KEY_ID_Mute,        KEY_ID_NONE,
};
/* settings, guide, bookmark index in Ir_Override_Table_Index_G10 */
const uint8_t Ir_Table_Correspend_Index_G10[] = {12, 13, 6};

/* Key Mapping Table Definition, need to align with hardware key matrix */
const T_KEY_INDEX_DEF KEY_MAPPING_TABLE_G20[KEYPAD_MAX_ROW_SIZE_G20][KEYPAD_MAX_COLUMN_SIZE_G20] =
{
    {KEY_ID_Power,  KEY_ID_5,           KEY_ID_0,       KEY_ID_Bookmark,    KEY_ID_DPadRight,   KEY_ID_Mute,        KEY_ID_APP03},
    {KEY_ID_Input,  KEY_ID_6,           KEY_ID_Info,    KEY_ID_Assistant,   KEY_ID_DPadDown,    KEY_ID_ChannelUp,   KEY_ID_APP04},
    {KEY_ID_1,      KEY_ID_7,           KEY_ID_Red,     KEY_ID_Settings,    KEY_ID_Back,        KEY_ID_VolDown,     KEY_ID_NONE},
    {KEY_ID_2,      KEY_ID_8,           KEY_ID_Green,   KEY_ID_DPadUp,      KEY_ID_Home,        KEY_ID_ChannelDown, KEY_ID_NONE},
    {KEY_ID_3,      KEY_ID_9,           KEY_ID_Yellow,  KEY_ID_DPadLeft,    KEY_ID_Guide,       KEY_ID_Youtube,     KEY_ID_NONE},
    {KEY_ID_4,      KEY_ID_Subtitles,   KEY_ID_Blue,    KEY_ID_DPadCenter,  KEY_ID_VolUp,       KEY_ID_Netflix,     KEY_ID_NONE},
};
/* settings, guide, subtitle, bookmark, red, green, yellow, blue index in KEY_MAPPING_TABLE_G20 */
const uint8_t Kb_Map_devicelayout_Correspend_Index_G20[] =  {18, 32, 36, 3, 16, 23, 30, 37};
const uint8_t Wakeup_Map_Correspend_Index_G20[] =
{
    KEY_ID_Assistant, KEY_ID_Home, KEY_ID_Power, KEY_ID_Input, KEY_ID_Youtube, KEY_ID_Netflix, KEY_ID_APP03, KEY_ID_APP04, KEY_ID_Blue,
    KEY_ID_Green, KEY_ID_Red, KEY_ID_Yellow
};
uint8_t Ir_Override_Table_Index_G20[48] =
{
    KEY_ID_Power,       KEY_ID_DPadRight,   KEY_ID_VolDown,     KEY_ID_Info,       KEY_ID_4,        KEY_ID_Green,
    KEY_ID_Input,       KEY_ID_DPadDown,    KEY_ID_ChannelDown, KEY_ID_0,          KEY_ID_3,        KEY_ID_Red,
    KEY_ID_Bookmark,    KEY_ID_Back,        KEY_ID_Youtube,     KEY_ID_Subtitles,  KEY_ID_2,        KEY_ID_6,
    KEY_ID_Assistant,   KEY_ID_Home,        KEY_ID_Netflix,     KEY_ID_9,          KEY_ID_1,        KEY_ID_5,
    KEY_ID_Settings,    KEY_ID_Guide,       KEY_ID_APP03,       KEY_ID_8,          KEY_ID_Blue,     KEY_ID_NONE,
    KEY_ID_DPadUp,      KEY_ID_VolUp,       KEY_ID_APP04,       KEY_ID_7,          KEY_ID_Yellow,   KEY_ID_NONE,
    KEY_ID_DPadLeft,    KEY_ID_ChannelUp,   KEY_ID_NONE,        KEY_ID_NONE,       KEY_ID_NONE,     KEY_ID_NONE,
    KEY_ID_NONE,        KEY_ID_NONE,        KEY_ID_NONE,        KEY_ID_NONE,       KEY_ID_NONE,     KEY_ID_NONE,
};
/* settings, guide, subtitle, bookmark, red, green, yellow, blue index in Ir_Override_Table_Index_G20 */
const uint8_t Ir_Table_Correspend_Index_G20[] = {24, 25, 15, 12, 11, 5, 34, 28};


/* BLE HID code table definition */
const T_KEY_CODE_DEF KEY_CODE_TABLE[KEY_CODE_TABLE_SIZE] =
{
    /* key_type,            hid_usage_page,         hid_usage_id,      ir_key_code */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_POWER,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_POWER, .ir_cmd2 = ~IR_POWER}
    },    /* KEY_ID_Power */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_RIGHT,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_RIGHT, .ir_cmd2 = ~IR_RIGHT}
    },    /* KEY_ID_DPadRight */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_VOLUMEDOWN,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VOL_DN, .ir_cmd2 = ~IR_VOL_DN}
    },    /* KEY_ID_VolDown */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_INFO,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_INFO, .ir_cmd2 = ~IR_INFO}
    },    /* KEY_ID_Info */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_4,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_4, .ir_cmd2 = ~IR_VK_4}
    },    /* KEY_ID_4 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_GREEN,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_GREEN, .ir_cmd2 = ~IR_GREEN}
    },    /* KEY_ID_Green */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_INPUT,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_INPUT, .ir_cmd2 = ~IR_INPUT}
    },    /* KEY_ID_Input */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_DN,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_DN, .ir_cmd2 = ~IR_DN}
    },    /* KEY_ID_DPadDown */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_CHN_DN,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_CHN_DN, .ir_cmd2 = ~IR_CHN_DN}
    },    /* KEY_ID_ChannelDown */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_0,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_0, .ir_cmd2 = ~IR_VK_0}
    },    /* KEY_ID_0 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_3,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_3, .ir_cmd2 = ~IR_VK_3}
    },    /* KEY_ID_3 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_RED,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_RED, .ir_cmd2 = ~IR_RED}
    },    /* KEY_ID_Red */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_BOOKMARK,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_BOOKMARK, .ir_cmd2 = ~IR_BOOKMARK}
    },    /* KEY_ID_Bookmark */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_BACK,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_BACK, .ir_cmd2 = ~IR_BACK}
    },    /* KEY_ID_Back */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_YOUTUBE,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_YOUTUBE, .ir_cmd2 = ~IR_YOUTUBE}
    },    /* KEY_ID_Youtube */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_SUBTITLE,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_SUBTITLE, .ir_cmd2 = ~IR_SUBTITLE}
    },    /* KEY_ID_Subtitles */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_2,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_2, .ir_cmd2 = ~IR_VK_2}
    },    /* KEY_ID_2 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_6,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_6, .ir_cmd2 = ~IR_VK_6}
    },    /* KEY_ID_6 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_ASSIST,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_ASSIST, .ir_cmd2 = ~IR_ASSIST}
    },    /* KEY_ID_Assistant */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_HOME,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_HOME, .ir_cmd2 = ~IR_HOME}
    },    /* KEY_ID_Home */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_NETFLIX,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_NETFLIX, .ir_cmd2 = ~IR_NETFLIX}
    },    /* KEY_ID_Netflix */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_9,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_9, .ir_cmd2 = ~IR_VK_9}
    },    /* KEY_ID_9 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_1,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_1, .ir_cmd2 = ~IR_VK_1}
    },    /* KEY_ID_1 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_5,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_5, .ir_cmd2 = ~IR_VK_5}
    },    /* KEY_ID_5 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_DASHBOARD,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_DASHBOARD, .ir_cmd2 = ~IR_DASHBOARD}
    },    /* KEY_ID_Dashboard */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_GUIDE,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_GUIDE, .ir_cmd2 = ~IR_GUIDE}
    },    /* KEY_ID_Guide */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_APP03,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_APP03, .ir_cmd2 = ~IR_APP03}
    },    /* KEY_ID_APP03 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_8,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_8, .ir_cmd2 = ~IR_VK_8}
    },    /* KEY_ID_8 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_BLUE,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_BLUE, .ir_cmd2 = ~IR_BLUE}
    },    /* KEY_ID_Blue */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_ALLAPPS,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_ALLAPPS, .ir_cmd2 = ~IR_ALLAPPS}
    },    /* KEY_ID_AllApps */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_UP,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_UP, .ir_cmd2 = ~IR_UP}
    },    /* KEY_ID_DPadUp */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_VOLUMEUP,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VOL_UP, .ir_cmd2 = ~IR_VOL_UP}
    },    /* KEY_ID_VolUp */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_APP04,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_APP04, .ir_cmd2 = ~IR_APP04}
    },    /* KEY_ID_APP04 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_KEYBOARD_PAGE,      G00GLE_VK_7,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_VK_7, .ir_cmd2 = ~IR_VK_7}
    },    /* KEY_ID_APP04 */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_YELLOW,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_YELLOW, .ir_cmd2 = ~IR_YELLOW}
    },    /* KEY_ID_Yellow */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_PROFILE,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_USERPROFILE, .ir_cmd2 = ~IR_USERPROFILE}
    },    /* KEY_ID_Profile */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_LEFT,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_LEFT, .ir_cmd2 = ~IR_LEFT}
    },    /* KEY_ID_DPadLeft */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_CHN_UP,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_CHN_UP, .ir_cmd2 = ~IR_CHN_UP}
    },    /* KEY_ID_ChannelUp */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_SETTINGS,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_SETTINGS, .ir_cmd2 = ~IR_SETTINGS}
    },    /* KEY_ID_Settings */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_LIVETV,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_LIVETV, .ir_cmd2 = ~IR_LIVETV}
    },    /* KEY_ID_Live */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_VOL_MUTE,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_MUTE, .ir_cmd2 = ~IR_MUTE}
    },    /* KEY_ID_Mute */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_FASTREWIND,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_FASTREWIND, .ir_cmd2 = ~IR_FASTREWIND}
    },    /* KEY_ID_FastRewind */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_CENTER,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_CENTER, .ir_cmd2 = ~IR_CENTER}
    },    /* KEY_ID_DPadCenter */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_RECORD,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_RECORD, .ir_cmd2 = ~IR_RECORD}
    },    /* KEY_ID_Record */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_PLAYPAUSE,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_PLAYPAUSE, .ir_cmd2 = ~IR_PLAYPAUSE}
    },    /* KEY_ID_PlayPause */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_FASTFORWARD,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_FASTFORWARD, .ir_cmd2 = ~IR_FASTFORWARD}
    },    /* KEY_ID_FastForward */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_MKEY_TELETEXT,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_TELETEXT, .ir_cmd2 = ~IR_TELETEXT}
    },    /* KEY_ID_TEXT */
    {
        KEY_TYPE_IR_ONLY,      HID_UNDEFINED_PAGE,      0,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_BUGREPORT, .ir_cmd2 = (uint8_t)~IR_BUGREPORT}
    },    /* KEY_ID_BugReport */
    {
        KEY_TYPE_BLE_AND_IR,    HID_CONSUMER_PAGE,      G00GLE_SHORTCUT,
        {.ir_addr1 = DEFAULT_IR_NEC_ADDR_1, .ir_addr2 = DEFAULT_IR_NEC_ADDR_2, .ir_cmd1 = IR_SHORTCUT, .ir_cmd2 = (uint8_t)~IR_SHORTCUT}
    },    /* KEY_ID_SHORTCUT */

    {
        KEY_TYPE_NONE,      HID_UNDEFINED_PAGE,      0,
        {.ir_addr1 = 0, .ir_addr2 = 0, .ir_cmd1 = 0, .ir_cmd2 = ~0}
    },    /* KEY_ID_NONE */
};

T_KEY_HANDLE_GLOBAL_DATA key_handle_global_data;  /* Value to indicate the reconnection key data */
TimerHandle_t combine_keys_detection_timer;
TimerHandle_t notify_key_data_after_reconn_timer;
#if FEATURE_SUPPORT_KEY_LONG_PRESS_PROTECT
TimerHandle_t long_press_key_detect_timer;
#endif
/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
static bool key_handle_check_navy_key(T_KEYSCAN_DATA keyscan_data);
static bool key_handle_prepare_hid_usage_buffer(T_KEY_INDEX_DEF key_index,
                                                T_KEY_HID_USAGES_BUFFER *p_buf);
static bool key_handle_notify_hid_usage_buffer(T_KEY_HID_USAGES_BUFFER *p_cur_buf,
                                               T_KEY_HID_USAGES_BUFFER *p_prev_buf);
static void key_handle_one_key_scenario(T_KEY_INDEX_DEF key_index);
static bool key_handle_detect_special_keys(T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data);
static void key_handle_process_repairing_event(void);
static void key_handle_process_factory_reset_event(void);

static void key_handle_comb_keys_timer_cb(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
static void notify_key_data_timer_cb(TimerHandle_t p_timer) DATA_RAM_FUNCTION;

static void key_handle_inqueue_key_pressed_cache_fifo(uint8_t key_index);
static uint8_t key_handle_dequeue_key_pressed_cache_fifo(void);


#if FEATURE_SUPPORT_KEY_LONG_PRESS_PROTECT
static void long_press_key_detect_timer_cb(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
#endif
/*============================================================================*
 *                              Local Functions
 *============================================================================*/
/******************************************************************
 * @brief  handle key prepare hid usage buffer
 * @param  p_cur_buf - point of current hid usage buffer
 * @param  p_prev_buf - point of previous hid usage buffer
 * @return bool - true or false
 * @retval void
 */
bool key_handle_prepare_hid_usage_buffer(T_KEY_INDEX_DEF key_index, T_KEY_HID_USAGES_BUFFER *p_buf)
{
    bool result = false;
    uint8_t buffer_index;
    uint8_t hid_usage_page = key_handle_get_hid_usage_page_by_index(key_index);
    uint16_t hid_usage_id = key_handle_get_hid_usage_id_by_index(key_index);
    T_KEY_TYPE_DEF key_type = key_handle_get_key_type_by_index(key_index);

    if ((KEY_TYPE_BLE_ONLY == key_type)
        || (KEY_TYPE_BLE_AND_IR == key_type)
        || (KEY_TYPE_BLE_OR_IR == key_type)
        || (KEY_TYPE_IR_PROGRAMMED == key_type))
    {
        if (HID_KEYBOARD_PAGE == hid_usage_page)
        {
            if (p_buf->keyboard_usage_cnt < MAX_HID_KEYBOARD_USAGE_CNT)
            {
                /* start from keyboard_usage_buffer[2] to keep same behavior as Bee1 and Bee2 */
                buffer_index = (p_buf->keyboard_usage_cnt + 2) % MAX_HID_KEYBOARD_USAGE_CNT;
                p_buf->keyboard_usage_buffer[buffer_index] = (uint8_t)hid_usage_id;
                p_buf->keyboard_usage_cnt += 1;

                result = true;
            }
            else
            {
                APP_PRINT_WARN0("[key_handle_prepare_hid_usage_buffer] keyboard_usage_buffer is full");
                result = false;
            }
        }
        else if (HID_CONSUMER_PAGE == hid_usage_page)
        {
            if (p_buf->consumer_usage_cnt < MAX_HID_CONSUMER_USAGE_CNT)
            {
                buffer_index = p_buf->consumer_usage_cnt;
                p_buf->consumer_usage_buffer[buffer_index] = hid_usage_id;
                p_buf->consumer_usage_cnt += 1;

                result = true;
            }
            else
            {
                APP_PRINT_WARN0("[key_handle_prepare_hid_usage_buffer] keyboard_usage_buf is full");
                result = false;
            }
        }
    }

    APP_PRINT_INFO2("[key_handle_prepare_hid_usage_buffer] key_index is %d, result is %d", key_index,
                    result);

    return result;
}

/******************************************************************
 * @brief  handle key notify hid usage buffer
 * @param  p_cur_buf - point of current hid usage buffer
 * @param  p_prev_buf - point of previous hid usage buffer
 * @return bool - true or false
 * @retval void
 */
bool key_handle_notify_hid_usage_buffer(T_KEY_HID_USAGES_BUFFER *p_cur_buf,
                                        T_KEY_HID_USAGES_BUFFER *p_prev_buf)
{
    bool result = false;

    /* check parameters and status */
    if ((p_cur_buf == NULL) || (p_prev_buf == NULL))
    {
        APP_PRINT_WARN0("[key_handle_notify_hid_usage_buffer] Invalid parameters");
        return false;
    }
    if (app_global_data.rcu_status != RCU_STATUS_PAIRED)
    {
        APP_PRINT_WARN1("[key_handle_notify_hid_usage_buffer] Invalid RCU Status %d",
                        app_global_data.rcu_status);
        return false;
    }

    /* check whether notification is needed or not */
    if ((true == app_global_data.is_keyboard_notify_en) &&
        (0 != memcmp(p_cur_buf->keyboard_usage_buffer, p_prev_buf->keyboard_usage_buffer,
                     sizeof(p_cur_buf->keyboard_usage_buffer))))
    {
        result = server_send_data(0, app_global_data.hid_srv_id, GATT_SRV_HID_KB_INPUT_INDEX,
                                  p_cur_buf->keyboard_usage_buffer, sizeof(p_cur_buf->keyboard_usage_buffer),
                                  GATT_PDU_TYPE_NOTIFICATION);
        if (result)
        {
            APP_PRINT_INFO1("[key_handle_notify_hid_usage_buffer] send keyboard_usage_buffer: 0x %b",
                            TRACE_BINARY(sizeof(p_cur_buf->keyboard_usage_buffer), p_cur_buf->keyboard_usage_buffer));
        }
    }

    if ((true == app_global_data.is_mm_keyboard_notify_en) &&
        (0 != memcmp(p_cur_buf->consumer_usage_buffer, p_prev_buf->consumer_usage_buffer,
                     sizeof(p_cur_buf->consumer_usage_buffer))))
    {
        result = server_send_data(0, app_global_data.hid_srv_id, GATT_SRV_HID_MM_KB_INPUT_INDEX,
                                  (uint8_t *)p_cur_buf->consumer_usage_buffer, sizeof(p_cur_buf->consumer_usage_buffer),
                                  GATT_PDU_TYPE_NOTIFICATION);
        if (result)
        {
            APP_PRINT_INFO1("[key_handle_notify_hid_usage_buffer] send consumer_usage_buffer: 0x %b",
                            TRACE_BINARY(sizeof(p_cur_buf->consumer_usage_buffer), p_cur_buf->consumer_usage_buffer));
        }
    }

    return result;
}

/******************************************************************
 * @brief  rest key pressed cache FIFO
 * @param  void
 * @return none
 * @retval void
 */
void key_handle_reset_key_pressed_cache_fifo(void)
{
    APP_PRINT_INFO0("[key_handle_reset_key_pressed_cache_queue] reset queue");
    memset(Key_Press_Cache_FIFO, (uint8_t)KEY_ID_NONE, sizeof(Key_Press_Cache_FIFO));
    current_cache_fifo_items_cnt = 0;
    cache_fifo_output_cnt = 0;
}

/******************************************************************
 * @brief  inqueue key pressed cache FIFO
 * @param  key_index - pressed key index
 * @return none
 * @retval void
 */
void key_handle_inqueue_key_pressed_cache_fifo(uint8_t key_index)
{
    bool result = false;

    if ((key_index < KEY_ID_NONE)
        && (current_cache_fifo_items_cnt < MAX_KEY_PRESS_CACHE_FIFO_CNT))
    {
        if ((key_index == KEY_ID_Power) &&
            (false == app_global_data.en_powerkey_cache))
        {
            result = false;
        }
        else
        {
            bool is_exist_flag = false;
            /* check whether key is exist or not */
            for (uint8_t index = 0; index < current_cache_fifo_items_cnt; index++)
            {
                if (key_index == Key_Press_Cache_FIFO[index])
                {
                    is_exist_flag = true;
                    break;
                }
            }

            if (false == is_exist_flag)
            {
                Key_Press_Cache_FIFO[current_cache_fifo_items_cnt++] = key_index;
            }
        }
    }

    APP_PRINT_INFO3("[key_handle_inqueue_key_pressed_cache_fifo] result = %d, key_index = %d, cnt = %d",
                    result, key_index, current_cache_fifo_items_cnt);
}

/******************************************************************
 * @brief  dequeue key pressed cache FIFO
 * @param  void
 * @return uint8_t
 * @retval void
 */
uint8_t key_handle_dequeue_key_pressed_cache_fifo(void)
{
    uint8_t key_index = KEY_ID_NONE;

    if (cache_fifo_output_cnt < current_cache_fifo_items_cnt)
    {
        key_index = Key_Press_Cache_FIFO[cache_fifo_output_cnt++];
    }

    APP_PRINT_INFO2("[key_handle_dequeue_key_pressed_cache_fifo] key_index = %d, cache_fifo_output_cnt = %d",
                    key_index, cache_fifo_output_cnt);

    return key_index;
}

/******************************************************************
 * @brief  handle one key pressed scenario
 * @param  key_index - pressed key index
 * @return none
 * @retval void
 */
void key_handle_one_key_scenario(T_KEY_INDEX_DEF key_index)
{
    T_KEY_TYPE_DEF key_type = key_handle_get_key_type_by_index(key_index);

    APP_PRINT_INFO3("[key_handle_one_key_scenario] key_index = %d, key_type = %d, rcu_status = %d",
                    key_index, key_type, app_global_data.rcu_status);
    if (key_index == KEY_ID_Back)
    {
        if (app_global_data.is_allow_to_factory_reset_for_back_key == 1)
        {
            APP_PRINT_INFO0("[key_handle_one_key_scenario] Back key pressed when HW reset and ready to factory reset.");
            key_handle_process_factory_reset_event();
        }
    }
    if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
    {
        if (key_handle_global_data.prev_keyscan_fifo_data.len == 1)
        {
            /* key change within one keyscan interval */
            key_handle_release_event();
        }

        if (key_index == KEY_ID_Assistant)
        {
            voice_handle_mic_key_pressed();
        }
        else
        {
            if (key_type == KEY_TYPE_IR_PROGRAMMED)
            {
                if (true == ir_service_handle_is_key_suppressed(key_index))
                {
                    key_handle_notify_hid_key_event_by_index(key_index);
                    LED_BLINK(LED_TYPE_BLINK_KEY_PRESS_BLE, 1);
                }
                else
                {
                    ir_send_key_press_handle(key_index, RAW_PROTOCOL);
                    ir_service_handle_notify_ir_key_event(key_index, KEY_ACTION_DOWN);
                    key_handle_global_data.ir_prog_key_down_id = key_index;
                    LED_BLINK(LED_TYPE_BLINK_KEY_PRESS_PROG, 1);
                }
            }
            else if (cache_fifo_output_cnt < current_cache_fifo_items_cnt)
            {
                /* cache key if pending key queue is not empty */
                key_handle_inqueue_key_pressed_cache_fifo(key_index);
            }
            else
            {
                key_handle_notify_hid_key_event_by_index(key_index);
                LED_BLINK(LED_TYPE_BLINK_KEY_PRESS_BLE, 1);
            }

            if (key_index == KEY_ID_DPadCenter)
            {
                voice_handle_atv_dpad_select();
            }
        }
    }
    else
    {
        if ((app_global_data.rcu_status == RCU_STATUS_ADVERTISING)
            && (app_global_data.adv_type == ADV_UNDIRECT_PAIRING)
            && (key_index == KEY_ID_Back))
        {
            APP_PRINT_INFO0("[key_handle_one_key_scenario] Back key pressed, stop pairing adv.");
            rcu_stop_adv(STOP_ADV_REASON_BACKKEY);
            LED_BLINK_EXIT(LED_TYPE_BLINK_PAIR_ADV);
        }

        /* check whether wake up adv is triggered or not */
        if (true == app_global_data.is_link_key_existed)
        {
            if (true == app_custom_is_wakeup_key(key_index))
            {
                if (app_global_data.rcu_status == RCU_STATUS_IDLE)
                {
                    app_global_data.wakeup_key_index = key_index;
                    app_global_data.wakeup_key_count++;
                    app_save_wakeup_key_cnt(app_global_data.wakeup_key_count);
                    rcu_start_adv(ADV_UNDIRECT_WAKEUP);
                }
                else if (app_global_data.rcu_status == RCU_STATUS_ADVERTISING)
                {
                    if ((app_global_data.adv_type == ADV_DIRECT_LDC)
                        || (app_global_data.adv_type == ADV_DIRECT_HDC)
                        || (app_global_data.adv_type == ADV_UNDIRECT_RECONNECT))
                    {
                        app_global_data.wakeup_key_index = key_index;
                        app_global_data.wakeup_key_count++;
                        app_save_wakeup_key_cnt(app_global_data.wakeup_key_count);
                        rcu_stop_adv(STOP_ADV_REASON_WAKEUPKEY);
                    }
                }
            }
            else
            {
                if (app_global_data.rcu_status == RCU_STATUS_IDLE)
                {
                    rcu_start_adv(ADV_DIRECT_LDC);
                }
                else if (app_global_data.rcu_status == RCU_STATUS_ADVERTISING)
                {
                    if ((app_global_data.adv_type == ADV_DIRECT_LDC)
                        || (app_global_data.adv_type == ADV_UNDIRECT_RECONNECT)
                        || (app_global_data.adv_type == ADV_DIRECT_LDC_LT))
                    {
                        rcu_stop_adv(STOP_ADV_REASON_RESTART_RECONNECT_ADV);
                    }
                }
            }
        }

        if ((key_index == KEY_ID_DPadCenter)
            || (key_index == KEY_ID_Back))
        {
            APP_PRINT_INFO0("[key_handle_one_key_scenario] delay IR post");
            key_handle_global_data.combine_keys_status = IR_DELAY_TX_KEYS_BIT_MASK;
            os_timer_restart(&combine_keys_detection_timer, IR_DELAY_TX_TIMEOUT);
        }
        else
        {
            if (key_type == KEY_TYPE_IR_PROGRAMMED)
            {
                ir_send_key_press_handle(key_index, RAW_PROTOCOL);
                LED_BLINK(LED_TYPE_BLINK_KEY_PRESS_PROG, 1);
            }
            else if ((key_type == KEY_TYPE_IR_ONLY)
                     || (key_type == KEY_TYPE_BLE_AND_IR)
                     || (key_type == KEY_TYPE_BLE_OR_IR))
            {
                ir_send_key_press_handle(key_index, NEC_PROTOCOL);
                LED_BLINK(LED_TYPE_BLINK_KEY_PRESS_IR, 1);
            }

            if ((key_type == KEY_TYPE_BLE_ONLY)
                || (key_type == KEY_TYPE_BLE_AND_IR)
                || (key_type == KEY_TYPE_IR_PROGRAMMED))
            {
                if (true == app_global_data.is_link_key_existed)
                {
                    key_handle_inqueue_key_pressed_cache_fifo(key_index);
                }
            }
        }
    }
}

/******************************************************************
 * @brief  key handle process repairing event
 * @param  none
 * @return none
 */
void key_handle_process_repairing_event(void)
{
    key_handle_reset_key_pressed_cache_fifo();

#if FEAUTRE_SUPPORT_IR_OVER_BLE
    ir_service_handle_reset_ir_table();
#endif

    app_global_data.pair_failed_retry_cnt = 0;
    switch (app_global_data.rcu_status)
    {
    case RCU_STATUS_IDLE:
        {
            rcu_start_adv(ADV_UNDIRECT_PAIRING);
        }
        break;
    case RCU_STATUS_ADVERTISING:
        {
            if (app_global_data.adv_type == ADV_UNDIRECT_PAIRING)
            {
                if (true == app_global_data.is_link_key_existed)
                {
                    os_timer_restart(&adv_timer, ADV_UNDIRECT_REPAIRING_TIMEOUT);
                }
                else
                {
                    os_timer_restart(&adv_timer, ADV_UNDIRECT_PAIRING_TIMEOUT);
                }
            }
            else
            {
                os_timer_stop(&adv_timer);
                rcu_stop_adv(STOP_ADV_REASON_PAIRING);
            }
        }
        break;
    case RCU_STATUS_PAIRED:
        {
            rcu_terminate_connection(DISCONN_REASON_PAIRING);
        }
        break;
    default:
        /* do nothing */
        break;
    }
}

/******************************************************************
 * @brief  key handle process factory reset event
 * @param  none
 * @return none
 */
void key_handle_process_factory_reset_event(void)
{
    if (bat_get_current_mode() == BAT_MODE_LOW_POWER)
    {
        APP_PRINT_INFO0("LOW POWER INDICATION: CAN NOT ENTER FACTORY RESET");
        return;
    }
    bool result = true;

#if FEATURE_SUPPORT_MP_TEST_MODE
    if (true == mp_test_is_test_mode_flag_en())
    {
        result = mp_test_disable_test_mode_flag();
    }
#endif

    /* reset FTL section */
    if (app_global_data.is_link_key_existed)
    {
        app_global_data.is_link_key_existed = false;
        le_bond_clear_all_keys();
    }

#if FEAUTRE_SUPPORT_IR_OVER_BLE
    ir_service_handle_invalid_ftl_table();
#endif

    app_global_data.wakeup_key_count = 0;
    app_save_wakeup_key_cnt(app_global_data.wakeup_key_count);

#if SUPPORT_LED_INDICATION_FEATURE
    if (result)
    {
        LED_BLINK(LED_TYPE_BLINK_CONFIRMATION, 2);
    }
    else
    {
        LED_BLINK(LED_TYPE_BLINK_ERROR, 4);
    }
#endif
    os_timer_restart(&system_rest_timer, SYSTEM_RESET_TIMEOUT);;
}

/******************************************************************
 * @brief  key handle process shortcut event
 * @param  none
 * @return none
 */
void key_handle_process_access_shortcut_event(void)
{
#if SUPPORT_LED_INDICATION_FEATURE
    LED_BLINK(LED_TYPE_BLINK_CONFIRMATION, 2);
#endif
    if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
    {
        APP_PRINT_INFO0("shortcut_event: in BLE");
    }
    else
    {
        APP_PRINT_INFO0("shortcut_event: in IR");
        ir_send_data_one_time_handle(KEY_ID_SHORTCUT, NEC_PROTOCOL);
    }
}
/******************************************************************
 * @brief  key handle process bug report event
 * @param  none
 * @return none
 */
void key_handle_process_bug_report_event(void)
{
#if SUPPORT_IR_TX_FEATURE
    if (app_global_data.rcu_status != RCU_STATUS_PAIRED &&
        app_global_data.rcu_status != RCU_STATUS_CONNECTED)
    {
        ir_send_data_one_time_handle(KEY_ID_BugReport, NEC_PROTOCOL);
    }
#endif

#if SUPPORT_LED_INDICATION_FEATURE
    LED_BLINK(LED_TYPE_BLINK_CONFIRMATION, 2);
#endif
}

/******************************************************************
 * @brief  key handle process ir delay tx event
 * @param  none
 * @return none
 */
void key_handle_process_ir_delay_tx_event(void)
{
    if ((keyscan_global_data.cur_fifo_data.len == 1)
        && (app_global_data.rcu_status != RCU_STATUS_PAIRED))
    {
        T_KEYSCAN_DATA keyscan_data = keyscan_global_data.cur_fifo_data.key[0];
        T_KEY_INDEX_DEF key_index =
            key_handle_global_data.cur_key_mapping_table[keyscan_data.row][keyscan_data.column];

        if ((key_index == KEY_ID_DPadCenter)
            || (key_index == KEY_ID_Back))
        {
            ir_send_key_press_handle(key_index, NEC_PROTOCOL);
            LED_BLINK(LED_TYPE_BLINK_KEY_PRESS_IR, 1);

            if (true == app_global_data.is_link_key_existed)
            {
                key_handle_inqueue_key_pressed_cache_fifo(key_index);
            }
        }
    }
}

/******************************************************************
 * @brief  key handle detect combine keys
 * @param  p_keyscan_fifo_data - the point of keyscan fifo data
 * @return bool - true or false
 * @retval void
 */
bool key_handle_detect_special_keys(T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data)
{
    bool result = false;
    uint32_t timeout_ms = COMBINE_KEYS_DETECT_TIMEOUT;

    if ((p_keyscan_fifo_data->len == 2)
        && (true == key_handle_is_key_index_in_fifo(KEY_ID_Home, p_keyscan_fifo_data))
        && (true == key_handle_is_key_index_in_fifo(KEY_ID_Back, p_keyscan_fifo_data)))
    {
        key_handle_global_data.combine_keys_status = PAIRING_COMBINE_KEYS_BIT_MASK;
        LED_BLINK(LED_TYPE_BLINK_COMB_KEY_PRESS, 0);
    }
    else if ((p_keyscan_fifo_data->len == 2)
             && (true == key_handle_is_key_index_in_fifo(KEY_ID_DPadCenter, p_keyscan_fifo_data))
             && (true == key_handle_is_key_index_in_fifo(KEY_ID_Mute, p_keyscan_fifo_data)))
    {
        key_handle_global_data.combine_keys_status = FACTORY_RESET_COMBINE_KEYS_BIT_MASK;
        /* update timeout period */
        timeout_ms = FACTORY_RESET_KEYS_DETECT_TIMEOUT;
        LED_BLINK(LED_TYPE_BLINK_COMB_KEY_PRESS, 0);
    }
    else if ((p_keyscan_fifo_data->len == 2)
             && (true == key_handle_is_key_index_in_fifo(KEY_ID_DPadCenter, p_keyscan_fifo_data))
             && (true == key_handle_is_key_index_in_fifo(KEY_ID_Back, p_keyscan_fifo_data)))
    {
        key_handle_global_data.combine_keys_status = BUG_REPORT_COMBINE_KEYS_BIT_MASK;

        /* update timeout period */
        timeout_ms = BUG_REPOERT_DETECT_TIMEOUT;
        LED_BLINK(LED_TYPE_BLINK_COMB_KEY_PRESS, 0);
        if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
        {
            app_global_data.allow_to_send_two_ble_key_value = true;
        }
    }
    else if ((p_keyscan_fifo_data->len == 2)
             && (true == key_handle_is_key_index_in_fifo(KEY_ID_DPadDown, p_keyscan_fifo_data))
             && (true == key_handle_is_key_index_in_fifo(KEY_ID_Back, p_keyscan_fifo_data)))
    {
        key_handle_global_data.combine_keys_status = ACCESS_SHORTCUT_COMBINE_KEYS_BIT_MASK;
        /* update timeout period */
        timeout_ms = ACCESS_SHORTCUT_DETECT_TIMEOUT;
        LED_BLINK(LED_TYPE_BLINK_COMB_KEY_PRESS, 0);
        if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
        {
            app_global_data.allow_to_send_two_ble_key_value = true;
        }
    }
    else if ((p_keyscan_fifo_data->len == 2)
             && (true == key_handle_check_navy_key(p_keyscan_fifo_data->key[0]))
             && (true == key_handle_check_navy_key(p_keyscan_fifo_data->key[1])))
    {
        if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
        {
            APP_PRINT_INFO0("in BLE mode:[key_handle_check_navy_key] 2 keys are all navi keys");
            app_global_data.allow_to_send_two_ble_key_value = true;
        }
        else
        {
            APP_PRINT_INFO0("in IR mode:[key_handle_check_navy_key] 2 keys are all navi keys, force len to 1!");
            p_keyscan_fifo_data->len = 1;
        }
    }

    APP_PRINT_INFO1("[key_handle_detect_special_keys] combine_keys_status is 0x%04X",
                    key_handle_global_data.combine_keys_status);
    if (key_handle_global_data.combine_keys_status != INVALID_COMBINE_KEYS_BIT_MASK)
    {
        result = true;
        /* start combine keys dectecion timer */
        os_timer_restart(&combine_keys_detection_timer, timeout_ms);
    }

    return result;
}

/******************************************************************
 * @brief    key handle combine keys detected timer callback
 * @param    p_timer - point of timer
 * @return   none
 * @retval   void
 * @note     do NOT execute time consumption functions in timer callback
 */
void key_handle_comb_keys_timer_cb(TimerHandle_t p_timer)
{
    /* check combine keys status */
    APP_PRINT_INFO1("[key_handle_comb_keys_timer_cb] combine_keys_status is 0x%04X",
                    key_handle_global_data.combine_keys_status);

    LED_BLINK_EXIT(LED_TYPE_BLINK_COMB_KEY_PRESS);

    if (PAIRING_COMBINE_KEYS_BIT_MASK == key_handle_global_data.combine_keys_status)
    {
        key_handle_process_repairing_event();
    }
    else if (FACTORY_RESET_COMBINE_KEYS_BIT_MASK == key_handle_global_data.combine_keys_status)
    {
        key_handle_process_factory_reset_event();
    }
    else if (BUG_REPORT_COMBINE_KEYS_BIT_MASK == key_handle_global_data.combine_keys_status)
    {
        key_handle_process_bug_report_event();
    }
    else if (ACCESS_SHORTCUT_COMBINE_KEYS_BIT_MASK == key_handle_global_data.combine_keys_status)
    {
        key_handle_process_access_shortcut_event();
    }
    else if (IR_DELAY_TX_KEYS_BIT_MASK == key_handle_global_data.combine_keys_status)
    {
        key_handle_process_ir_delay_tx_event();
    }

#if FEATURE_SUPPORT_MP_TEST_MODE
    mp_test_handle_comb_keys_timer_cb(key_handle_global_data.combine_keys_status);
#endif
}

/******************************************************************
 * @brief    notify key data after reconn timer callback
 * @param    p_timer - point of timer
 * @return   none
 * @retval   void
 * @note     do NOT execute time consumption functions in timer callback
 */
void notify_key_data_timer_cb(TimerHandle_t p_timer)
{
    APP_PRINT_INFO2("[notify_key_data_timer_cb] rcu_status is %d, cached fifo length is %d",
                    app_global_data.rcu_status, current_cache_fifo_items_cnt);

    if ((app_global_data.rcu_status == RCU_STATUS_PAIRED) &&
        (cache_fifo_output_cnt < current_cache_fifo_items_cnt))
    {
        uint8_t key_index = key_handle_dequeue_key_pressed_cache_fifo();

        if (key_index == KEY_ID_NONE)
        {
            key_handle_reset_key_pressed_cache_fifo();
        }
        else
        {
            if (KEY_TYPE_IR_PROGRAMMED == key_handle_get_key_type_by_index((T_KEY_INDEX_DEF) key_index))
            {
                if (false == ir_service_handle_is_key_suppressed(key_index))
                {
                    ir_service_handle_notify_ir_key_event(key_index, KEY_ACTION_DOWN);
                    ir_service_handle_notify_ir_key_event(key_index, KEY_ACTION_UP);
                }
                else
                {
                    key_handle_notify_hid_key_event_by_index((T_KEY_INDEX_DEF)key_index);
                    key_handle_notify_hid_release_event();
                }
            }
            else
            {
                key_handle_notify_hid_key_event_by_index((T_KEY_INDEX_DEF)key_index);
                key_handle_notify_hid_release_event();
            }

            os_timer_restart(&notify_key_data_after_reconn_timer, NOTIFY_KEY_DATA_INTERVAL_TIMEOUT);
        }
    }
    else
    {
        key_handle_reset_key_pressed_cache_fifo();
    }
}

/******************************************************************
 * @brief    long press key detect timer callback
 * @param    p_timer - point of timer
 * @return   none
 * @retval   void
 */
void long_press_key_detect_timer_cb(TimerHandle_t p_timer)
{
    APP_PRINT_INFO0("[long_press_key_detect_timer_cb] detect key long pressed event");
    key_handle_release_event();
    key_handle_global_data.is_key_long_pressed = true;
}

/*============================================================================*
 *                              Global Functions
 *============================================================================*/
/******************************************************************
 * @brief    Get key type by key index
 * @param    key_index - key index
 * @return   T_KEY_TYPE_DEF - key type
 * @retval   void
 */
T_KEY_TYPE_DEF key_handle_get_key_type_by_index(T_KEY_INDEX_DEF key_index)
{
    return key_handle_global_data.cur_key_code_table[key_index].key_type;
}

/******************************************************************
 * @brief    Get IR key code by key index
 * @param    key_index - key index
 * @return   uint8_t - ir key code
 * @retval   void
 */
IR_KEY_CODE key_handle_get_ir_key_code_by_index(T_KEY_INDEX_DEF key_index)
{
    return key_handle_global_data.cur_key_code_table[key_index].ir_key_code;
}

/******************************************************************
 * @brief    Get HID usage page by key index
 * @param    key_index - key index
 * @return   uint8_t - hid usage page
 * @retval   void
 */
uint8_t key_handle_get_hid_usage_page_by_index(T_KEY_INDEX_DEF key_index)
{
    return key_handle_global_data.cur_key_code_table[key_index].hid_usage_page;
}

/******************************************************************
 * @brief    Get HID usage id by key index
 * @param    key_index - key index
 * @return   uint16_t - hid usage id
 * @retval   void
 */
uint16_t key_handle_get_hid_usage_id_by_index(T_KEY_INDEX_DEF key_index)
{
    return key_handle_global_data.cur_key_code_table[key_index].hid_usage_id;
}

/******************************************************************
 * @brief    Initialize key handle data
 * @param    dev_type - device type
 * @return   none
 * @retval   void
 */
void key_handle_init_data(void)
{
    APP_PRINT_INFO0("[key_handle_init_data] init data");
    memset(&key_handle_global_data, 0, sizeof(key_handle_global_data));

    /* set default keyscan parameters */
    key_handle_global_data.keyscan_row_size = KEYPAD_MAX_ROW_SIZE_G10;
    key_handle_global_data.keyscan_column_size = KEYPAD_MAX_COLUMN_SIZE_G10;
    key_handle_global_data.ir_prog_key_down_id = KEY_ID_NONE;
    memcpy(key_handle_global_data.cur_key_mapping_table, KEY_MAPPING_TABLE_G10,
           sizeof(KEY_MAPPING_TABLE_G10));
    memcpy(key_handle_global_data.cur_key_code_table, KEY_CODE_TABLE, sizeof(KEY_CODE_TABLE));

    key_handle_reset_key_pressed_cache_fifo();
}

/******************************************************************
 * @brief    Set key typte to KEY_TYPE_IR_PROGRAMMED by key_id
 * @param    key_id - key id
 * @return   none
 * @retval   void
 */
void key_handle_set_key_type_prog_by_id(uint8_t key_id)
{
    APP_PRINT_INFO1("[key_handle_set_key_type_prog_by_id] key_id = %d", key_id);
    key_handle_global_data.cur_key_code_table[key_id].key_type = KEY_TYPE_IR_PROGRAMMED;
}

/******************************************************************
 * @brief    Restore key typte by key_id
 * @param    key_id - key id
 * @return   none
 * @retval   void
 */
void key_handle_restore_key_type_id(uint8_t key_id)
{
    APP_PRINT_INFO1("[key_handle_restore_key_type_id] key_id = %d", key_id);
    key_handle_global_data.cur_key_code_table[key_id].key_type = KEY_CODE_TABLE[key_id].key_type;
}

/******************************************************************
 * @brief    key handler init timer
 * @param    none
 * @return   none
 * @retval   void
 */
void key_handle_init_timer(void)
{
    APP_PRINT_INFO0("[key_handle_init_timer] init timer");
    /* combine_keys_detection_timer is used to detect combine keys after timeout */
    if (false == os_timer_create(&combine_keys_detection_timer, "combine_keys_detection_timer",  1, \
                                 COMBINE_KEYS_DETECT_TIMEOUT, false, key_handle_comb_keys_timer_cb))
    {
        APP_PRINT_ERROR0("[key_handle_init_timer] combine_keys_detection_timer creat failed!");
    }

    /* notify_key_data_after_reconn_timer is used to notify key data after timeout */
    if (false == os_timer_create(&notify_key_data_after_reconn_timer,
                                 "notify_key_data_after_reconn_timer",  1, \
                                 NOTIFY_KEY_DATA_TIMEOUT, false, notify_key_data_timer_cb))
    {
        APP_PRINT_ERROR0("[key_handle_init_timer] notify_key_data_after_reconn_timer creat failed!");
    }
#if FEATURE_SUPPORT_KEY_LONG_PRESS_PROTECT
    if (false == os_timer_create(&long_press_key_detect_timer, "long_press_key_detect_timer",  1, \
                                 LONG_PRESS_KEY_DETECT_TIMEOUT, false, long_press_key_detect_timer_cb))
    {
        APP_PRINT_ERROR0("[key_handle_init_timer] long_press_key_detect_timer creat failed!");
    }
#endif
}

/******************************************************************
 * @brief  key handle check specific key index is in keyscan FIFO or not
 * @param  key_index - key index
 * @param  p_keyscan_fifo_data - the point of keyscan fifo data
 * @return bool - true or false
 * @retval void
 */
bool key_handle_is_key_index_in_fifo(T_KEY_INDEX_DEF key_index,
                                     T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data)
{
    for (uint32_t loop_index = 0; loop_index < p_keyscan_fifo_data->len; loop_index++)
    {
        if (key_index ==
            key_handle_global_data.cur_key_mapping_table[p_keyscan_fifo_data->key[loop_index].row][p_keyscan_fifo_data->key[loop_index].column])
        {
            return true;
        }
    }

    return false;
}

/******************************************************************
 * @brief  key handle check specific key index is in navi key or not
 * @param  key_index - key index
 * @return bool - true or false
 * @retval void
 */
bool key_handle_check_navy_key(T_KEYSCAN_DATA keyscan_data)
{
    T_KEY_INDEX_DEF key_index =
        key_handle_global_data.cur_key_mapping_table[keyscan_data.row][keyscan_data.column];

    if ((key_index == KEY_ID_DPadUp) || (key_index == KEY_ID_DPadDown) ||
        (key_index == KEY_ID_DPadLeft) || (key_index == KEY_ID_DPadRight))
    {
        return true;
    }

    return false;
}

/******************************************************************
 * @brief    key handle notify hid key event by index
 * @param    key_index - index of key
 * @return   bool - true of false
 * @retval   none
 */
bool key_handle_notify_hid_key_event_by_index(T_KEY_INDEX_DEF key_index)
{
    bool result = false;

    memset(&key_handle_global_data.current_hid_usage_buf, 0, sizeof(T_KEY_HID_USAGES_BUFFER));

    if (true == key_handle_prepare_hid_usage_buffer(key_index,
                                                    &key_handle_global_data.current_hid_usage_buf))
    {
        if (true == key_handle_notify_hid_usage_buffer(&key_handle_global_data.current_hid_usage_buf,
                                                       &key_handle_global_data.prev_hid_usage_buf))
        {
            memcpy(&key_handle_global_data.prev_hid_usage_buf, &key_handle_global_data.current_hid_usage_buf,
                   sizeof(T_KEY_HID_USAGES_BUFFER));
            result = true;

            if (ATVV_VERSION_0_4 == atvv_global_data.app_support_version)
            {
                if (key_index == KEY_ID_DPadCenter)
                {
                    bool notify_result = false;
                    memset(atvv_global_data.char_ctl_data_buff, 0, ATVV_CHAR_CTL_DATA_LEN);
                    atvv_global_data.char_ctl_data_buff[0] = ATV_CTL_OPCODE_DPAD_SELECT;

                    notify_result = server_send_data(0, app_global_data.atvv_srv_id, GATT_SVC_ATVV_CHAR_CTL_VALUE_INDEX,
                                                     atvv_global_data.char_ctl_data_buff, 1, GATT_PDU_TYPE_NOTIFICATION);
                    if (notify_result == false)
                    {
                        APP_PRINT_WARN0("[key_handle_notify_hid_key_event_by_index] ATV_CTL_OPCODE_DPAD_SELECT notify failed!");
                    }
                    else
                    {
                        APP_PRINT_INFO0("[key_handle_notify_hid_key_event_by_index] ATV_CTL_OPCODE_DPAD_SELECT notify success!");
                    }
                }
            }
        }
    }

    APP_PRINT_INFO2("[key_handle_notify_hid_key_event_by_index] key_index is %d, result = %d",
                    key_index, result);

    return result;
}

/******************************************************************
 * @brief    key handle notify hid key event by keyscan fifo
 * @param    p_keyscan_fifo_data - point of keyscan fifo data
 * @return   bool - true of false
 * @retval   none
 */
bool key_handle_notify_hid_key_event_by_fifo(T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data)
{
    bool result = false;
    T_KEY_INDEX_DEF key_index;
    T_KEY_TYPE_DEF key_type;

    memset(&key_handle_global_data.current_hid_usage_buf, 0, sizeof(T_KEY_HID_USAGES_BUFFER));

    for (uint8_t loop_index = 0; loop_index < p_keyscan_fifo_data->len; loop_index++)
    {
        key_index =
            key_handle_global_data.cur_key_mapping_table[p_keyscan_fifo_data->key[loop_index].row][p_keyscan_fifo_data->key[loop_index].column];
        key_type = key_handle_global_data.cur_key_code_table[key_index].key_type;

        if ((key_type == KEY_TYPE_BLE_ONLY)
            || (key_type == KEY_TYPE_BLE_AND_IR)
            || (key_type == KEY_TYPE_BLE_OR_IR))
        {
            if (true == key_handle_prepare_hid_usage_buffer(key_index,
                                                            &key_handle_global_data.current_hid_usage_buf))
            {
                /* get invalid hid usage */
                result = true;
            }
        }
    }

    if (true == result)
    {
        if (true == key_handle_notify_hid_usage_buffer(&key_handle_global_data.current_hid_usage_buf,
                                                       &key_handle_global_data.prev_hid_usage_buf))
        {
            memcpy(&key_handle_global_data.prev_hid_usage_buf, &key_handle_global_data.current_hid_usage_buf,
                   sizeof(T_KEY_HID_USAGES_BUFFER));
            result = true;
        }
        else
        {
            result = false;
        }
    }

    APP_PRINT_INFO2("[key_handle_notify_hid_key_event_by_fifo] len is %d, result = %d",
                    p_keyscan_fifo_data->len, result);

    return result;
}

/******************************************************************
 * @brief    key handle notify hid release event
 * @param    void
 * @return   bool - true of false
 * @retval   none
 */
bool key_handle_notify_hid_release_event(void)
{
    bool result = false;

    memset(&key_handle_global_data.current_hid_usage_buf, 0, sizeof(T_KEY_HID_USAGES_BUFFER));

    if (true == key_handle_notify_hid_usage_buffer(&key_handle_global_data.current_hid_usage_buf,
                                                   &key_handle_global_data.prev_hid_usage_buf))
    {
        memcpy(&key_handle_global_data.prev_hid_usage_buf, &key_handle_global_data.current_hid_usage_buf,
               sizeof(T_KEY_HID_USAGES_BUFFER));
        result = true;
    }

    return result;
}

/******************************************************************
 * @brief    handle key pressed event
 * @param    p_keyscan_fifo_data - point of keyscan FIFO data
 * @return   none
 * @retval   void
 */
void key_handle_pressed_event(T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data)
{
    key_handle_global_data.key_pressed_count++;

    T_KEYSCAN_FIFO_DATA keyscan_fifo_data;
    memcpy(&keyscan_fifo_data, p_keyscan_fifo_data, sizeof(T_KEYSCAN_FIFO_DATA));
    APP_PRINT_INFO2("[key_handle_pressed_event] keyscan FIFO length is %d, prev length is %d",
                    keyscan_fifo_data.len, key_handle_global_data.prev_keyscan_fifo_data.len);
    for (uint8_t index = 0; index < (keyscan_fifo_data.len); index++)
    {
        APP_PRINT_INFO4("[key_handle_pressed_event] keyscan data[%d]: row - %d, column - %d, value - %d", \
                        index, keyscan_fifo_data.key[index].row, keyscan_fifo_data.key[index].column,
                        key_handle_global_data.cur_key_mapping_table[keyscan_fifo_data.key[index].row][keyscan_fifo_data.key[index].column]);
    }

#if SUPPORT_IR_TX_FEATURE
    if (true == ir_send_is_working())
    {
        ir_send_key_release_handle();
    }
#endif

#if SUPPORT_VOICE_FEATURE
    if (true == voice_driver_global_data.is_voice_driver_working)
    {
        voice_handle_mic_key_released();
    }
#endif

#if SUPPORT_BUZZER_FEATURE
    if (true == buzzer_is_working_check())
    {
        buzzer_stop_pwm_output();
        fms_handle_notify_status(FMS_STATUS_OK, FMS_ERROR_NO_ERR);
    }
#endif

#if SUPPORT_BAT_DETECT_FEATURE
    bat_msg_handle(IO_MSG_BAT_DETECT_KEY_PRESSED);

    if (bat_get_current_mode() == BAT_MODE_LOW_POWER)
    {
        APP_PRINT_INFO0("LOW POWER LED INDICATION");
        LED_BLINK(LED_TYPE_BLINK_TEST_RED, 5);
    }

    if (BAT_MODE_POWER_DOWN == bat_get_current_mode())
    {
        return;
    }
#endif

#if FEATURE_SUPPORT_KEY_LONG_PRESS_PROTECT
    os_timer_start(&long_press_key_detect_timer);
#endif

    /* detect special combine keys scenario */
    key_handle_detect_special_keys(&keyscan_fifo_data);

    if (keyscan_fifo_data.len == 0)
    {
        APP_PRINT_WARN0("[key_handle_pressed_event] FIFO length is 0!");
        key_handle_release_event();
    }
    else if (keyscan_fifo_data.len == 1)
    {
        if (key_handle_global_data.prev_keyscan_fifo_data.len == 0)
        {
            T_KEY_INDEX_DEF key_index_1 =
                key_handle_global_data.cur_key_mapping_table[keyscan_fifo_data.key[0].row][keyscan_fifo_data.key[0].column];

            key_handle_one_key_scenario(key_index_1);
        }
    }
    else
    {
        /* multiple keys event senario */
#if FEATURE_SUPPORT_REPORT_MULTIPLE_HID_KEY_CODES
        if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
        {
            if (app_global_data.allow_to_send_two_ble_key_value == true)
            {
                APP_PRINT_INFO0("key_handle_notify_hid_key_event_by_fifo:allow to send two ble keys");
                app_global_data.allow_to_send_two_ble_key_value = false;
                key_handle_notify_hid_key_event_by_fifo(&keyscan_fifo_data);
            }
        }
#else
        if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
        {
            key_handle_notify_hid_release_event();
        }
#endif
    }

    memcpy(&key_handle_global_data.prev_keyscan_fifo_data, &keyscan_fifo_data,
           sizeof(T_KEYSCAN_FIFO_DATA));
}

/******************************************************************
 * @brief    handle key release event
 * @param    none
 * @return   none
 * @retval   void
 */
void key_handle_release_event(void)
{
    APP_PRINT_INFO0("[key_handle_release_event] key release event");

    memset(&key_handle_global_data.prev_keyscan_fifo_data, 0, sizeof(T_KEYSCAN_FIFO_DATA));

#if FEATURE_SUPPORT_KEY_LONG_PRESS_PROTECT
    os_timer_stop(&long_press_key_detect_timer);
    key_handle_global_data.is_key_long_pressed = false;
#endif

#if SUPPORT_IR_TX_FEATURE
    if (true == ir_send_is_working())
    {
        ir_send_key_release_handle();
    }
#endif

#if SUPPORT_VOICE_FEATURE
    if (voice_driver_global_data.is_voice_driver_working == true)
    {
        voice_handle_mic_key_released();
    }
    else
#endif
        if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
        {
            if (key_handle_global_data.ir_prog_key_down_id != KEY_ID_NONE)
            {
                ir_service_handle_notify_ir_key_event(key_handle_global_data.ir_prog_key_down_id, KEY_ACTION_UP);
                key_handle_global_data.ir_prog_key_down_id = KEY_ID_NONE;
            }

            key_handle_notify_hid_release_event();
        }
}

/******************************************************************
 * @brief    handle key release event
 * @param    none
 * @return   none
 * @retval   void
 */
void key_handle_disconnect_event(void)
{
    key_handle_reset_key_pressed_cache_fifo();
}

/******************************************************************
 * @brief    Get key pressed count
 * @param    none
 * @return   uint32_t
 * @retval   key_pressed_count
 */
uint32_t key_handle_get_key_pressed_count(void)
{
    return key_handle_global_data.key_pressed_count;
}

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
