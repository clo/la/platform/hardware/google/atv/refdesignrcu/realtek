/*****************************************************************************************
*     Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
*****************************************************************************************
  * @file    key_handle.h
  * @brief   This file contains all the constants and functions prototypes for key handle.
  * @details
  * @author  barry_bian
  * @date    2020-02-25
  * @version v1.0
  * *************************************************************************************
  */

#ifndef _KEY_HANDLE_H_
#define _KEY_HANDLE_H_

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include <stdint.h>
#include <stdbool.h>
#include <keyscan_driver.h>

/*============================================================================*
 *                              Macro Definitions
 *============================================================================*/
/* define the bit mask of combine keys */
#define INVALID_COMBINE_KEYS_BIT_MASK               0x0000
#define PAIRING_COMBINE_KEYS_BIT_MASK               0x0001
#define IR_LEARNING_COMBINE_KEYS_BIT_MASK           0x0002
#define HCI_UART_TEST_COMBINE_KEYS_BIT_MASK         0x0004
#define DATA_UART_TEST_COMBINE_KEYS_BIT_MASK        0x0008
#define SINGLE_TONE_TEST_COMBINE_KEYS_BIT_MASK      0x0010
#define FAST_PAIR_1_COMBINE_KEYS_BIT_MASK           0x0020
#define FAST_PAIR_2_COMBINE_KEYS_BIT_MASK           0x0040
#define FAST_PAIR_3_COMBINE_KEYS_BIT_MASK           0x0080
#define FAST_PAIR_4_COMBINE_KEYS_BIT_MASK           0x0100
#define FAST_PAIR_5_COMBINE_KEYS_BIT_MASK           0x0200
#define BUG_REPORT_COMBINE_KEYS_BIT_MASK            0x0400
#define ACCESS_SHORTCUT_COMBINE_KEYS_BIT_MASK       0x0800
#define FACTORY_RESET_COMBINE_KEYS_BIT_MASK         0x1000
#define IR_DELAY_TX_KEYS_BIT_MASK                   0x2000

#define COMBINE_KEYS_DETECT_TIMEOUT         4000  /* 4 sec */
#define COMBINE_KEYS_TEST_DETECT_TIMEOUT    2000  /* 2 sec */
#define FACTORY_RESET_KEYS_DETECT_TIMEOUT   4000  /* 4 sec */
#define BUG_REPOERT_DETECT_TIMEOUT          1000  /* 1 sec */
#define ACCESS_SHORTCUT_DETECT_TIMEOUT      1000  /* 1 sec */
#define NOTIFY_KEY_DATA_TIMEOUT             100   /* 100 ms */
#define NOTIFY_KEY_DATA_INTERVAL_TIMEOUT    20    /* 20 ms */
#define IR_DELAY_TX_TIMEOUT                 100   /* 100 ms */
#define LONG_PRESS_KEY_DETECT_TIMEOUT       30000 /* 30 sec */

#define MAX_HID_KEYBOARD_USAGE_CNT      8
#define MAX_HID_CONSUMER_USAGE_CNT      3

#define DEFAULT_IR_NEC_ADDR_1     0x88
#define DEFAULT_IR_NEC_ADDR_2     0x77

#define IR_VK_1                               0x01
#define IR_VK_2                               0x02
#define IR_VK_3                               0x03
#define IR_VK_4                               0x04
#define IR_VK_5                               0x05
#define IR_VK_6                               0x06
#define IR_VK_7                               0x07
#define IR_VK_8                               0x08
#define IR_VK_9                               0x09
#define IR_VK_0                               0x0A
#define IR_SETTINGS                           0x0F
#define IR_DASHBOARD                          0x10
#define IR_UP                                 0x15
#define IR_DN                                 0x16
#define IR_LEFT                               0x17
#define IR_RIGHT                              0x18
#define IR_CENTER                             0x19
#define IR_POWER                              0x21
#define IR_VOL_UP                             0x23
#define IR_VOL_DN                             0x24
#define IR_MUTE                               0x25
#define IR_INFO                               0x29
#define IR_GUIDE                              0x32
#define IR_CHN_UP                             0x33
#define IR_CHN_DN                             0x34
#define IR_ASSIST                             0x46
#define IR_HOME                               0x47
#define IR_BACK                               0x48
#define IR_YELLOW                             0x49
#define IR_GREEN                              0x4a
#define IR_RED                                0x4b
#define IR_BLUE                               0x4c
#define IR_FASTREWIND                         0x51
#define IR_PLAYPAUSE                          0x52
#define IR_FASTFORWARD                        0x53
#define IR_RECORD                             0x54
#define IR_ALLAPPS                            0x57
#define IR_SUBTITLE                           0x58
#define IR_USERPROFILE                        0x59
#define IR_INPUT                              0x60
#define IR_LIVETV                             0x61
#define IR_NETFLIX                            0x63
#define IR_YOUTUBE                            0x64
#define IR_APP03                              0x67
#define IR_APP04                              0x68
#define IR_BOOKMARK                           0x74
#define IR_TELETEXT                           0x75
#define IR_BUGREPORT                          0x96
#define T_VK_CH_UP                            0xd0
#define T_VK_CH_DN                            0xd1
#define IR_SHORTCUT                           0xD0
/*============================================================================*
 *                              Types
 *============================================================================*/
typedef enum
{
    KEY_ACTION_DOWN = 0,
    KEY_ACTION_UP = 1,
} T_KEY_ACTION_TYPE;

/* define the key index */
typedef enum
{
    KEY_ID_Power = 0,
    KEY_ID_DPadRight = 1,
    KEY_ID_VolDown = 2,
    KEY_ID_Info = 3,
    KEY_ID_4 = 4,
    KEY_ID_Green = 5,
    KEY_ID_Input = 6,
    KEY_ID_DPadDown = 7,
    KEY_ID_ChannelDown = 8,
    KEY_ID_0 = 9,
    KEY_ID_3 = 10,
    KEY_ID_Red = 11,
    KEY_ID_Bookmark = 12,
    KEY_ID_Back = 13,
    KEY_ID_Youtube = 14,
    KEY_ID_Subtitles = 15,
    KEY_ID_2 = 16,
    KEY_ID_6 = 17,
    KEY_ID_Assistant = 18,
    KEY_ID_Home = 19,
    KEY_ID_Netflix = 20,
    KEY_ID_9 = 21,
    KEY_ID_1 = 22,
    KEY_ID_5 = 23,
    KEY_ID_Dashboard = 24,
    KEY_ID_Guide = 25,
    KEY_ID_APP03 = 26,
    KEY_ID_8 = 27,
    KEY_ID_Blue = 28,
    KEY_ID_AllApps = 29,
    KEY_ID_DPadUp = 30,
    KEY_ID_VolUp = 31,
    KEY_ID_APP04 = 32,
    KEY_ID_7 = 33,
    KEY_ID_Yellow = 34,
    KEY_ID_Profile = 35,
    KEY_ID_DPadLeft = 36,
    KEY_ID_ChannelUp = 37,
    KEY_ID_Settings = 38,
    KEY_ID_Live = 39,
    KEY_ID_Mute = 40,
    KEY_ID_FastRewind = 41,
    KEY_ID_DPadCenter = 42,
    KEY_ID_Record = 43,
    KEY_ID_PlayPause = 44,
    KEY_ID_FastForward = 45,
    KEY_ID_TEXT = 46,
    KEY_ID_BugReport = 47,
    KEY_ID_SHORTCUT = 48,
    KEY_ID_NONE = 49,

    KEY_INDEX_ENUM_GUAID = 50,

} T_KEY_INDEX_DEF;

/* define the key types */
typedef enum
{
    KEY_TYPE_NONE       = 0x00,  /* none key type */
    KEY_TYPE_BLE_ONLY   = 0x01,  /* only BLE key type */
    KEY_TYPE_IR_ONLY    = 0x02,  /* only IR key type */
    KEY_TYPE_BLE_AND_IR  = 0x03,  /* BLE and IR key type */
    KEY_TYPE_BLE_OR_IR   = 0x04,  /* BLE or IR key type */
    KEY_TYPE_IR_PROGRAMMED = 0x05,  /* programmed IR key type */
} T_KEY_TYPE_DEF;

/* define the HID usage pages */
typedef enum
{
    HID_UNDEFINED_PAGE  = 0x00,
    HID_KEYBOARD_PAGE   = 0x07,
    HID_CONSUMER_PAGE   = 0x0C,
} T_HID_USAGE_PAGES_DEF;

// media key, consumer key
//reference:  <<HID Usage Tables>>  Consumer Page(0x0C)
typedef enum
{
    G00GLE_VK_1         = 0x1e,
    G00GLE_VK_2         = 0x1f,
    G00GLE_VK_3         = 0x20,
    G00GLE_VK_4         = 0x21,
    G00GLE_VK_5         = 0x22,
    G00GLE_VK_6         = 0x23,
    G00GLE_VK_7         = 0x24,
    G00GLE_VK_8         = 0x25,
    G00GLE_VK_9         = 0x26,
    G00GLE_VK_0         = 0x27,
    G00GLE_VK_VOLUMEUP  = 0x80,
    G00GLE_VK_VOLUMEDOWN    = 0x81,
    G00GLE_SHORTCUT     = 0xD0,

//device_layout
    G00GLE_MKEY_NOTIFICATION = 0x009f,
    G00GLE_MKEY_LIVETV     = 0x0089,
    G00GLE_MKEY_TELETEXT   = 0x0185,
    G00GLE_MKEY_USERPROFILE = 0x0229,
    G00GLE_MKEY_ALLAPPS    = 0x01A2,
    G00GLE_MKEY_FASTREWIND = 0x00B4,
    G00GLE_MKEY_RECORD     = 0x00CE,
    G00GLE_MKEY_PLAYPAUSE  = 0x00CD,
    G00GLE_MKEY_FASTFORWARD = 0x00B3,

//g20
    G00GLE_MKEY_INFO       = 0x01BD,
    G00GLE_MKEY_SUBTITLE   = 0x0061,
    G00GLE_MKEY_RED        = 0x0069,
    G00GLE_MKEY_GREEN      = 0x006A,
    G00GLE_MKEY_YELLOW     = 0x006c,
    G00GLE_MKEY_BLUE       = 0x006b,
//g10
    G00GLE_MKEY_YOUTUBE    = 0x0077,
    G00GLE_MKEY_NETFLIX    = 0x0078,
    G00GLE_MKEY_APP03      = 0x0079,
    G00GLE_MKEY_APP04      = 0x007A,
    G00GLE_MKEY_UP         = 0x0042,
    G00GLE_MKEY_DN         = 0x0043,
    G00GLE_MKEY_LEFT       = 0x0044,
    G00GLE_MKEY_RIGHT      = 0x0045,
    G00GLE_MKEY_CENTER     = 0x0041,
    G00GLE_MKEY_HOME       = 0x0223,
    G00GLE_MKEY_BACK       = 0x0224,
    G00GLE_MKEY_POWER      = 0x0030,
    G00GLE_MKEY_VOL_MUTE   = 0X00E2,
    G00GLE_MKEY_CHN_UP     = 0x009C,
    G00GLE_MKEY_CHN_DN     = 0x009D,
    G00GLE_MKEY_GUIDE      = 0x008D,
    G00GLE_MKEY_BOOKMARK   = 0x022a,
    G00GLE_MKEY_ASSIST     = 0x0221,
    G00GLE_MKEY_INPUT      = 0x01bb,
    G00GLE_MKEY_SETTINGS   = 0x0096,
    G00GLE_MKEY_DASHBOARD  = 0x009F,
    G00GLE_MKEY_PROFILE    = 0x0229,
} google_media_key_t;

/* define the key code table size, the value should modify according to KEY_CODE_TABLE */
#define KEY_CODE_TABLE_SIZE KEY_INDEX_ENUM_GUAID

typedef union ir_key_code
{
    uint32_t ir_data;
    struct
    {
        uint8_t ir_addr1;
        uint8_t ir_addr2;
        uint8_t ir_cmd1;
        uint8_t ir_cmd2;
    };
} IR_KEY_CODE;


/* define the struct of key code */
typedef struct
{
    T_KEY_TYPE_DEF key_type;
    uint8_t hid_usage_page;
    uint16_t hid_usage_id;
    IR_KEY_CODE ir_key_code;
} T_KEY_CODE_DEF;

typedef struct
{
    uint8_t keyboard_usage_cnt;
    uint8_t keyboard_usage_buffer[MAX_HID_KEYBOARD_USAGE_CNT];
    uint8_t consumer_usage_cnt;
    uint16_t consumer_usage_buffer[MAX_HID_CONSUMER_USAGE_CNT];
} T_KEY_HID_USAGES_BUFFER;

/* Key global parameters' struct */
typedef struct
{
    bool is_key_long_pressed; /* to indicate whether key is long pressed */
    uint8_t keyscan_row_size;  /* to indicate the row size of keyscan */
    uint8_t keyscan_column_size;  /* to indicate the column size of keyscan */
    uint32_t combine_keys_status;  /* to indicate the status of combined keys */
    uint32_t key_pressed_count;  /* to indicate the count of key pressed */
    uint8_t ir_prog_key_down_id;  /* to indicate the index of IR programmed key down event */
    T_KEY_HID_USAGES_BUFFER current_hid_usage_buf;  /* to indicate the current key HID usage buffer */
    T_KEY_HID_USAGES_BUFFER prev_hid_usage_buf;  /* to indicate the preivous key HID usage buffer */
    T_KEYSCAN_FIFO_DATA prev_keyscan_fifo_data;  /* to indicateh the previous keyscan fifo data */
    T_KEY_INDEX_DEF
    cur_key_mapping_table[KEYPAD_MAX_ROW_SIZE][KEYPAD_MAX_COLUMN_SIZE];  /* to indicate the key mapping table */
    T_KEY_CODE_DEF cur_key_code_table[KEY_CODE_TABLE_SIZE];  /* to indicate current key code table */
} T_KEY_HANDLE_GLOBAL_DATA;

/*============================================================================*
*                           Export Global Variables
*============================================================================*/
extern const uint8_t Wakeup_Map_Correspend_Index_G10[];
extern const uint8_t Kb_Map_devicelayout_Correspend_Index_G10[];
extern const uint8_t Wakeup_Map_Correspend_Index_G20[];
extern const uint8_t Kb_Map_devicelayout_Correspend_Index_G20[];
extern uint8_t Ir_Override_Table_Index_G10[];
extern uint8_t Ir_Override_Table_Index_G20[];
extern const uint8_t Ir_Table_Correspend_Index_G10[];
extern const uint8_t Ir_Table_Correspend_Index_G20[];
extern const T_KEY_INDEX_DEF
KEY_MAPPING_TABLE_G20[KEYPAD_MAX_ROW_SIZE_G20][KEYPAD_MAX_COLUMN_SIZE_G20];
extern const T_KEY_INDEX_DEF
KEY_MAPPING_TABLE_G10[KEYPAD_MAX_ROW_SIZE_G10][KEYPAD_MAX_COLUMN_SIZE_G10];
extern T_KEY_HANDLE_GLOBAL_DATA key_handle_global_data;
extern TimerHandle_t combine_keys_detection_timer;
extern TimerHandle_t notify_key_data_after_reconn_timer;

/*============================================================================*
 *                          Functions
 *============================================================================*/
T_KEY_TYPE_DEF key_handle_get_key_type_by_index(T_KEY_INDEX_DEF key_index);
IR_KEY_CODE key_handle_get_ir_key_code_by_index(T_KEY_INDEX_DEF key_index);
uint8_t key_handle_get_hid_usage_page_by_index(T_KEY_INDEX_DEF key_index);
uint16_t key_handle_get_hid_usage_id_by_index(T_KEY_INDEX_DEF key_index);
void key_handle_init_data(void);
void key_handle_pressed_event(T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data);
void key_handle_release_event(void);
bool key_handle_is_key_index_in_fifo(T_KEY_INDEX_DEF key_index,
                                     T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data);
bool key_handle_notify_hid_key_event_by_index(T_KEY_INDEX_DEF key_index);
bool key_handle_notify_hid_key_event_by_fifo(T_KEYSCAN_FIFO_DATA *p_keyscan_fifo_data);
bool key_handle_set_pending_keyscan_fifo_data(T_KEYSCAN_FIFO_DATA *p_data);
void key_handle_reset_pending_keyscan_fifo_data(void);
bool key_handle_notify_pending_hid_buffer(void);
bool key_handle_notify_hid_release_event(void);
void key_handle_init_timer(void);
void key_handle_set_key_type_prog_by_id(uint8_t key_id);
void key_handle_restore_key_type_id(uint8_t key_id);
void key_handle_disconnect_event(void);
uint32_t key_handle_get_key_pressed_count(void);
void key_handle_reset_key_pressed_cache_fifo(void);

#ifdef __cplusplus
}
#endif

#endif
