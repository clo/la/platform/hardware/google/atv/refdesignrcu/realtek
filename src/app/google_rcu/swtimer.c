/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     swtimer.c
* @brief    software timers source file.
* @details  include initialization and callback funcitions of software timers
* @author
* @date     2020-02-24
* @version  v1.0
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "os_timer.h"
#include <trace.h>
#include "board.h"
#include "swtimer.h"
#include "rcu_gap.h"
#include "rcu_application.h"
#include "key_handle.h"
#include "keyscan_driver.h"
#include "gap_bond_le.h"
#include "rtl876x_keyscan.h"
#include "app_section.h"
#if SUPPORT_VOICE_FEATURE
#include "voice_driver.h"
#endif
#if SUPPORT_BUZZER_FEATURE
#include "fms_service_handle.h"
#endif

/*============================================================================*
 *                              Global Variables
 *============================================================================*/
TimerHandle_t adv_change_data_timer;
TimerHandle_t adv_timer;
TimerHandle_t temp_off_latency_timer;
#if FEATURE_SUPPORT_NO_ACTION_DISCONN
TimerHandle_t no_act_disconn_timer;
#endif
TimerHandle_t update_conn_params_timer;
TimerHandle_t next_state_check_timer;
#if (AON_WDG_ENABLE == 1)
void *aon_watch_dog_wake_up_dlps_timer;
#define AON_WDG_TIMER_WAKEUP_DLPS_PERIOD     ((AON_WDG_TIME_OUT_PERIOD_SECOND - 1) * 1000)
#endif
TimerHandle_t system_rest_timer;
TimerHandle_t back_key_pressed_when_HW_reset_timer;
/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
static void adv_timer_callback(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
static void adv_change_data_timer_callback(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
static void next_state_timeout_timer_callback(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
static void update_conn_params_timer_cb(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
#if FEATURE_SUPPORT_NO_ACTION_DISCONN
static void no_act_disconn_timer_callback(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
#endif
#if (AON_WDG_ENABLE == 1)
static void aon_watch_dog_wake_up_dlps_callback(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
#endif
static void system_reset_timer_callback(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
static void temp_off_latency_timer_cb(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
static void back_key_pressed_when_HW_reset_timer_cb(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
/*============================================================================*
 *                              Local Functions
 *============================================================================*/

/******************************************************************
* @brief   back_key_pressed_when_HW_reset_timer callback.
* @param   p_timer - point of timer
* @return  none
* @retval  void
* @note    if reset reason is HW and back key is pressed in 50ms, then clean pairing table(inserting battery)
*/
void back_key_pressed_when_HW_reset_timer_cb(TimerHandle_t p_timer)
{
    APP_PRINT_INFO0("back_key_pressed_when_HW_reset_timer_cb");
    app_global_data.is_allow_to_factory_reset_for_back_key = 0;
}

/******************************************************************
 * @brief   turn of slave latency timer callback.
 * @param   p_timer - point of timer
 * @return  none
 * @retval  void
 * @note    do NOT execute time consumption functions in timer callback
 */
void temp_off_latency_timer_cb(TimerHandle_t p_timer)
{
    if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
    {
        app_set_latency_status(LATENCY_TEMP_OFF_BIT, LANTENCY_ON);
    }
}

/******************************************************************
 * @brief advertising timer callback
 *
 * adv_timer_callback is used to stop advertising after timeout
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void adv_timer_callback(TimerHandle_t p_timer)
{
    if (app_global_data.rcu_status == RCU_STATUS_ADVERTISING)
    {
        if (app_global_data.adv_type != ADV_DIRECT_HDC)
        {
            rcu_stop_adv(STOP_ADV_REASON_TIMEOUT);
        }
    }
}

/******************************************************************
 * @brief advertising change data timer callback
 *
 * adv_change_data_timer_callback is used to change advertising data after timeout
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void adv_change_data_timer_callback(TimerHandle_t p_timer)
{
    APP_PRINT_INFO0("[adv_change_data_timer_callback] timeout");
    app_change_wakeup_adv_data();
}

/******************************************************************
 * @brief pair failed disconnection timer callback
 *
 * pair_fail_disconn_timer_callback is used to disconnect for pair failed
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void next_state_timeout_timer_callback(TimerHandle_t p_timer)
{
    if (app_global_data.rcu_status == RCU_STATUS_CONNECTED)
    {
        APP_PRINT_INFO0("[pair_fail_disconn_timer_callback] timeout");
        rcu_terminate_connection(DISCONN_REASON_PAIR_FAILED);
    }
}

/******************************************************************
 * @brief update connection parameters timer callback
 *
 * update_conn_params_timer is used to update desired connection parameters after timeout
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void update_conn_params_timer_cb(TimerHandle_t p_timer)
{
    if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
    {
#if SUPPORT_VOICE_FEATURE
        if (voice_driver_global_data.is_voice_driver_working == true)
        {
            /* voice module is working, start timer to attempt later */
            os_timer_restart(&update_conn_params_timer, UPDATE_CONN_PARAMS_TIMEOUT);
        }
        else
#endif
        {
            rcu_update_conn_params(RCU_CONNECT_INTERVAL, RCU_CONNECT_LATENCY,
                                   RCU_SUPERVISION_TIMEOUT);
            app_set_latency_status(LATENCY_SYS_UPDATE_BIT, LANTENCY_ON);  /* turn on latency */
        }
    }
    else
    {
        APP_PRINT_WARN1("[update_conn_params_timer_cb] Invalid rcu status: %d", app_global_data.rcu_status);
    }
}

#if FEATURE_SUPPORT_NO_ACTION_DISCONN
/******************************************************************
 * @brief no action disconnect timer callback
 *
 * no_act_disconn_timer_callback is used to terminate connection after timeout
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void no_act_disconn_timer_callback(TimerHandle_t p_timer)
{
    if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
    {
        APP_PRINT_INFO0("[RCU] Idle No Action Timeout, Disconnect.");
        rcu_terminate_connection(DISCONN_REASON_TIMEOUT);
    }
}
#endif

#if (AON_WDG_ENABLE == 1)
void aon_watch_dog_wake_up_dlps_callback(TimerHandle_t pxTimer)
{
    APP_PRINT_INFO0("aon_watch_dog_wake_up_dlps_callback timeout!");
}
#endif

/******************************************************************
 * @brief system reset timer callback
 *
 * adv_timer_callback is used to stop advertising after timeout
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void system_reset_timer_callback(TimerHandle_t p_timer)
{
    WDG_SystemReset(RESET_ALL_EXCEPT_AON, RESET_REASON_FACTORY_RESET);
}

/*============================================================================*
 *                              Global Functions
 *============================================================================*/
/******************************************************************
 * @brief software timer init function
 * @param  none
 * @return none
 * @retval void
 */
void sw_timer_init(void)
{
    /* back_key_pressed_when_HW_reset_timer is used to control reset by back key */
    if (false == os_timer_create(&back_key_pressed_when_HW_reset_timer,
                                 "back_key_pressed_when_HW_reset_timer",  1, \
                                 CHECK_BACK_KEY_PRESSED_WHEN_HW_RESET_TIMEOUT, false,
                                 back_key_pressed_when_HW_reset_timer_cb))
    {
        APP_PRINT_INFO0("[sw_timer_init] init back_key_pressed_when_HW_reset_timer failed");
    }

    /* adv_timer is used to stop advertising after timeout */
    if (false == os_timer_create(&adv_timer, "adv_timer",  1, \
                                 ADV_UNDIRECT_PAIRING_TIMEOUT, false, adv_timer_callback))
    {
        APP_PRINT_INFO0("[sw_timer_init] init adv_timer failed");
    }

    /* adv_change_data_timer is used to change advertising data after timeout */
    if (false == os_timer_create(&adv_change_data_timer, "adv_change_data_timer",  1, \
                                 ADV_CHANGE_DATA_TIMEOUT, false, adv_change_data_timer_callback))
    {
        APP_PRINT_INFO0("[sw_timer_init] init adv_change_data_timer failed");
    }

    /* pair_fail_disconn_timer is used to disconnect for pair failed */
    if (false == os_timer_create(&next_state_check_timer, "pair_fail_disconn_timer",  1, \
                                 PAIR_FAIL_DISCONN_TIMEOUT, false, next_state_timeout_timer_callback))
    {
        APP_PRINT_INFO0("[sw_timer_init] init pairing_exception_timer failed");
    }

#if FEATURE_SUPPORT_NO_ACTION_DISCONN
    /* no_act_disconn_timer is used to disconnect after timeout if there is on action under connection */
    if (false == os_timer_create(&no_act_disconn_timer, "no_act_disconn_timer",  1, \
                                 NO_ACTION_DISCON_TIMEOUT, false, no_act_disconn_timer_callback))
    {
        APP_PRINT_INFO0("[sw_timer_init] init no_act_disconn_timer failed");
    }
#endif

#if (AON_WDG_ENABLE == 1)
    if (false == os_timer_create(&aon_watch_dog_wake_up_dlps_timer, "aon_watch_dog_wake_up_dlps_timer",
                                 1, \
                                 AON_WDG_TIMER_WAKEUP_DLPS_PERIOD, true, aon_watch_dog_wake_up_dlps_callback))
    {
        APP_PRINT_INFO0("[sw_timer_init] init aon_watch_dog_wake_up_dlps_callback failed");
    }
    else
    {
        os_timer_start(&aon_watch_dog_wake_up_dlps_timer);
        APP_PRINT_INFO0("Start aon_watch_dog_wake_up_dlps_callback!");
    }
#endif

    /* update_conn_params_timer is used to update desired connection parameters after timeout */
    if (false == os_timer_create(&update_conn_params_timer, "update_conn_params_timer", 1
                                 , UPDATE_CONN_PARAMS_TIMEOUT, false, update_conn_params_timer_cb))
    {
        APP_PRINT_INFO0("[sw_timer_init] init update_conn_params_timer failed");
    }

    if (false == os_timer_create(&system_rest_timer, "system_rest_timer", 1
                                 , SYSTEM_RESET_TIMEOUT, false, system_reset_timer_callback))
    {
        APP_PRINT_INFO0("[sw_timer_init] init system_rest_timer failed");
    }

    if (false == os_timer_create(&temp_off_latency_timer, "temp_off_latency_timer", 1
                                 , TEMP_OFF_LANTENCY_TIMEOUT, false, temp_off_latency_timer_cb))
    {
        APP_PRINT_INFO0("[sw_timer_init] init temp_off_latency_timer failed");
    }

    keyscan_init_timer();

    key_handle_init_timer();

#if SUPPORT_BUZZER_FEATURE
    fms_handle_init_timer();
#endif
}

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
