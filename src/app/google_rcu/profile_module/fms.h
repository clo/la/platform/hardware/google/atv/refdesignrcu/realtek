/**
*****************************************************************************************
*     Copyright(c) 2015, Realtek Semiconductor Corporation. All rights reserved.
*****************************************************************************************
  * @file     fms.h
  * @brief    Head file for using Find me Service.
  * @details  Find Me service data structs and external functions declaration.
  * @author   Chenjie Jin
  * @date     2022-05-31
  * @version  v1.0
  * *************************************************************************************
  */

#ifndef _FMS_H_
#define _FMS_H_

#include "profile_server.h"

#ifdef __cplusplus
extern "C"  {
#endif      /* __cplusplus */


/** @brief  Demo Profile service related UUIDs. */
#define GATT_UUID128_FMS_SERVICE            0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0x01, 0x00, 0x02, 0x18
#define GATT_UUID128_FMS_CONTROL            0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0x02, 0x00, 0x02, 0x18
#define GATT_UUID128_FMS_DATA               0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0x03, 0x00, 0x02, 0x18


/** @brief  Index of each characteristic in Demo Profile service database. */
#define GATT_SVC_FMS_CONTROL_INDEX       2
#define GATT_SVC_FMS_DATA_INDEX          4
#define GATT_SVC_FMS_CCCD_INDEX          5

/** @brief  fms service write event */
#define FMS_WRITE_CHAR_CONTROL              1
#define FMS_WRITE_CHAR_DATA                 2

/** @brief  fms service write event length*/
#define FMS_WRITE_CHAR_CONTROL_LEN          2

/** @brief  fms service notification flag */
#define FMS_DATA_NOTIFY_ENABLE              1
#define FMS_DATA_NOTIFY_DISABLE             2

/** @brief  FMS_CTL_MODE */
#define FMS_CTL_MODE_DISABLE_ALL        0x0000  /* disable all features */
#define FMS_CTL_MODE_ALERT              0xE100  /* ready for Find me */

/** @brief  FMS_DATA_EVNET */
#define FMS_DATA_STOP_ALERT             0x0000  /* disable buzzer */
#define FMS_DATA_ALERT_LEVEL_LOW        0xE101  /* run the buzzer at low vol. */
#define FMS_DATA_ALERT_LEVEL_MEDIUM     0xE102  /* run the buzzer at medium vol. */
#define FMS_DATA_ALERT_LEVEL_HIGH       0xE103  /* run the buzzer at high vol. */

/** @brief  status or error type */
#define FMS_STATUS_OK                   0xE180
#define FMS_STATUS_ERROR                0x0F00

#define FMS_ERROR_NO_ERR                0x0000
#define FMS_ERROR_MODE_DISABLED         0x0F01
#define FMS_ERROR_UNKNOWN_CMD           0x0F02

/** Message content */
typedef union
{
    struct
    {
        uint16_t len;
        uint8_t *report;
    } report_data;
} T_FMS_WRITE_PARAMETER;

/** @struct T_FMS_WRITE_MSG
  * @brief write message
  */
typedef struct
{
    uint8_t write_type; /**< ref: @ref IR_Write_Info */
    T_FMS_WRITE_PARAMETER write_parameter;
} T_FMS_WRITE_MSG;

typedef union _FMS_UPSTREAM_MSG_DATA
{
    uint8_t read_value_index;
    uint8_t notification_indification_index;
    T_FMS_WRITE_MSG write_msg;
} T_FMS_UPSTREAM_MSG_DATA;

/** FMS service data to inform application */
typedef struct _T_FMS_CALLBACK_DATA
{
    T_SERVICE_CALLBACK_TYPE     msg_type;
    T_FMS_UPSTREAM_MSG_DATA    msg_data;
} T_FMS_CALLBACK_DATA;

uint8_t fms_add_service(void *pFunc);

#ifdef __cplusplus
}
#endif

#endif
