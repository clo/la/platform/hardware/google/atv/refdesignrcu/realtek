/**
*****************************************************************************************
*     Copyright(c) 2015, Realtek Semiconductor Corporation. All rights reserved.
*****************************************************************************************
  * @file     ir_service.h
  * @brief    Head file for using IR Service.
  * @details  IR service data structs and external functions declaration.
  * @author   barry_bian
  * @date     2022-01-11
  * @version  v1.0
  * *************************************************************************************
  */

#ifndef _IR_SERVICE_H_
#define _IR_SERVICE_H_

#include "profile_server.h"

#ifdef __cplusplus
extern "C"  {
#endif      /* __cplusplus */


/** @brief  Demo Profile service related UUIDs. */
#define GATT_UUID128_IR_SERVICE             0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0xC0, 0xBF, 0x43, 0xD3
#define GATT_UUID128_IR_PROG_CONTROL        0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0xC1, 0xBF, 0x43, 0xD3
#define GATT_UUID128_IR_KEY_ID              0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0xC2, 0xBF, 0x43, 0xD3
#define GATT_UUID128_IR_CODE                0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0xC3, 0xBF, 0x43, 0xD3
#define GATT_UUID128_IR_SUPPRESS            0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0xC4, 0xBF, 0x43, 0xD3
#define GATT_UUID128_IR_KEY_EVENT           0x64, 0xB6, 0x17, 0xF6, 0x01, 0xAF, 0x7D, 0xBC, 0x05, 0x4F, 0x21, 0x5A, 0xC5, 0xBF, 0x43, 0xD3

/** @brief  Index of each characteristic in Demo Profile service database. */
#define GATT_SVC_IR_PROG_CONTROL_INDEX      2
#define GATT_SVC_IR_KEY_ID_INDEX            4
#define GATT_SVC_IR_CODE_INDEX              6
#define GATT_SVC_IR_SUPPRESS_INDEX          8
#define GATT_SVC_IR_KEY_EVENT_INDEX         10
#define GATT_SVC_IR_KEY_EVENT_CCCD_INDEX    11

/** @brief  ir service write event */
#define IR_WRITE_PROG_CONTROL               1
#define IR_WRITE_KEY_ID                     2
#define IR_WRITE_CODE                       3
#define IR_WRITE_SUPPRESS                   4

/** @brief  ir service write event length*/
#define IR_WRITE_PROG_CONTROL_LEN           1
#define IR_WRITE_KEY_ID_LEN                 2

/** @brief  ir service notification flag */
#define IR_KEY_EVNET_NOTIFY_ENABLE          1
#define IR_KEY_EVNET_NOTIFY_DISABLE         2

/** @brief  IR_PROG_CONTROL event */
#define IR_PROG_CONTROL_START               0x01
#define IR_PROG_CONTROL_FINISH              0x00

/** @brief  IR_KEY_EVNET event */
#define IR_KEY_UP                           0x01
#define IR_KEY_DOWN                         0x00

/** Message content */
typedef union
{
    struct
    {
        uint16_t len;
        uint8_t *report;
    } report_data;
} T_IR_WRITE_PARAMETER;

/** @struct T_IR_WRITE_MSG
  * @brief write message
  */
typedef struct
{
    uint8_t write_type; /**< ref: @ref IR_Write_Info */
    T_IR_WRITE_PARAMETER write_parameter;
} T_IR_WRITE_MSG;

typedef union _IR_UPSTREAM_MSG_DATA
{
    uint8_t read_value_index;
    uint8_t notification_indification_index;
    T_IR_WRITE_MSG write_msg;
} T_IR_UPSTREAM_MSG_DATA;

/** IR service data to inform application */
typedef struct _T_IR_CALLBACK_DATA
{
    T_SERVICE_CALLBACK_TYPE     msg_type;
    T_IR_UPSTREAM_MSG_DATA    msg_data;
} T_IR_CALLBACK_DATA;

uint8_t ir_svc_add_service(void *pFunc);

#ifdef __cplusplus
}
#endif

#endif
