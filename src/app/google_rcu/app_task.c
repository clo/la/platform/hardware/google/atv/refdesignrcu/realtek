/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     app_task.c
* @brief    application task.
* @details
* @author   barry_bian
* @date     2020-02-21
* @version  v1.1
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "board.h"
#include <os_msg.h>
#include <os_task.h>
#include <trace.h>
#include <app_task.h>
#include <app_msg.h>
#include <app_task.h>
#include <rcu_application.h>
#include "mem_config.h"
#include "otp_config.h"
#include "app_section.h"
#include "rcu_gap_dtm.h"
#if (AON_WDG_ENABLE == 1)
#include "rtl876x_aon_wdg.h"
#endif

/*============================================================================*
 *                              Macros
 *============================================================================*/
#define APP_TASK_PRIORITY           1   /* Task priorities. */
#if (VOICE_ENC_TYPE == SW_OPUS_ENC)
#if SUPPORT_DUAL_MIC_FEATURE
#define APP_TASK_STACK_SIZE         512 * 60
#else
#define APP_TASK_STACK_SIZE         512 * 40
#endif

#else
#define APP_TASK_STACK_SIZE         512 * 4
#endif

#define MAX_NUMBER_OF_GAP_MESSAGE   0x20
#define MAX_NUMBER_OF_IO_MESSAGE    0x20
#define MAX_NUMBER_OF_EVENT_MESSAGE (MAX_NUMBER_OF_GAP_MESSAGE + MAX_NUMBER_OF_IO_MESSAGE)

/*============================================================================*
 *                          Global Variables
 *============================================================================*/
void *app_task_handle;
void *evt_queue_handle;
void *io_queue_handle;
void *dtm_task_handle;
/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
static void app_main_task(void *p_param);
void dtm_task(void *p_param);
bool app_send_msg_to_apptask(T_IO_MSG *p_msg) DATA_RAM_FUNCTION;
void app_main_task(void *p_param) DATA_RAM_FUNCTION;
void dtm_task(void *p_param) DATA_RAM_FUNCTION;
bool app_send_msg_to_apptask(T_IO_MSG *p_msg) DATA_RAM_FUNCTION;
void dtm_uart_init(void);
/*============================================================================*
 *                              Local Functions
 *============================================================================*/
/******************************************************************
 * @brief   handle function of app main task
 * @param   *p_param
 * @return  none
 * @retval  void
 */
void app_main_task(void *p_param)
{
    uint8_t event;

    os_msg_queue_create(&io_queue_handle, MAX_NUMBER_OF_IO_MESSAGE, sizeof(T_IO_MSG));
    os_msg_queue_create(&evt_queue_handle, MAX_NUMBER_OF_EVENT_MESSAGE, sizeof(uint8_t));

    gap_start_bt_stack(evt_queue_handle, io_queue_handle, MAX_NUMBER_OF_GAP_MESSAGE);

#if (ROM_WATCH_DOG_ENABLE == 1)
    extern void reset_watch_dog_timer_enable(void);
    reset_watch_dog_timer_enable();
#endif

#if (AON_WDG_ENABLE == 1)
    aon_wdg_init(1, AON_WDG_TIME_OUT_PERIOD_SECOND);
#endif

    while (true)
    {
        if (os_msg_recv(evt_queue_handle, &event, 0xFFFFFFFF) == true)
        {
            //DBG_DIRECT("***os_msg_recv***");
            if (event == EVENT_IO_TO_APP)
            {
                T_IO_MSG io_msg;
                if (os_msg_recv(io_queue_handle, &io_msg, 0) == true)
                {
                    app_handle_io_msg(io_msg);
                }
            }
            else
            {
                gap_handle_msg(event);
            }
        }
    }
}

/*============================================================================*
 *                              Global Functions
 *============================================================================*/
/******************************************************************
 * @brief    send msg queue to app task.
 *
 * @param    p_handle   The handle to the message queue being peeked.
 *
 * @return           The status of the message queue peek.
 * @retval true      Message queue was peeked successfully.
 * @retval false     Message queue was failed to peek.
 */
bool app_send_msg_to_apptask(T_IO_MSG *p_msg)
{
    uint8_t event = EVENT_IO_TO_APP;

    if (os_msg_send(io_queue_handle, p_msg, 0) == false)
    {
        APP_PRINT_ERROR0("send_io_msg_to_app fail");
        return false;
    }
    if (os_msg_send(evt_queue_handle, &event, 0) == false)
    {
        APP_PRINT_ERROR0("send_evt_msg_to_app fail");
        return false;
    }
    return true;
}

/******************************************************************
 * @brief   app_task_init() init app task
 * @param   none
 * @return  none
 * @retval  void
 */
void app_task_init(void)
{
    os_task_create(&app_task_handle, "app", app_main_task, 0, APP_TASK_STACK_SIZE,
                   APP_TASK_PRIORITY);
}

/******************************************************************
 * @brief   dtm_task_init() init dtm task
 * @param   none
 * @return  none
 * @retval  void
 */
void dtm_task_init(void)
{
    os_task_create(&dtm_task_handle, "dtm_task", dtm_task, 0, 512 * 4, 1);
}

/******************************************************************
 * @brief   handle function of dtm task
 * @param   *p_param
 * @return  none
 * @retval  void
 */
void dtm_task(void *p_param)
{
    uint8_t event;
    os_msg_queue_create(&io_queue_handle, MAX_NUMBER_OF_IO_MESSAGE, sizeof(T_IO_MSG));
    os_msg_queue_create(&evt_queue_handle, MAX_NUMBER_OF_EVENT_MESSAGE, sizeof(uint8_t));

    gap_start_bt_stack(evt_queue_handle, io_queue_handle, MAX_NUMBER_OF_GAP_MESSAGE);

    dtm_uart_init();

    while (true)
    {
        if (os_msg_recv(evt_queue_handle, &event, 0xffffffff) == true)
        {
            if (event == EVENT_IO_TO_APP)
            {
                T_IO_MSG io_msg;
                if (os_msg_recv(io_queue_handle, &io_msg, 0) == true)
                {
                    //dtm need not to handle io message
                }
            }
            else
            {
                gap_handle_msg(event);
            }
        }
    }
}
/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
