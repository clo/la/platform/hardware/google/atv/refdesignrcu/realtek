/**
*********************************************************************************************************
*               Copyright(c) 2018, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     fms_service_handle.c
* @brief    This is the entry of user code which the FMS service handle module resides in.
* @details
* @author   chenjie jin
* @date     2022-05-31
* @version  v1.0
*********************************************************************************************************
*/

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include <string.h>
#include "board.h"
#include "fms_service_handle.h"
#include "fms.h"
#include "trace.h"
#include "profile_server.h"
#include "rcu_application.h"
#include "os_timer.h"
#include "swtimer.h"
#include "os_mem.h"
#include "app_task.h"
#include "buzzer_driver.h"
#include "app_section.h"
#include "key_handle.h"
#include "rcu_gap.h"

#if SUPPORT_BUZZER_FEATURE

/*============================================================================*
 *                       Static Variables
 *============================================================================*/
static TimerHandle_t Fms_Periodical_Conn_Timer = NULL;
static bool Fms_Handle_Notify_Flag = false;
static uint16_t Fms_Current_Mode = FMS_CTL_MODE_ALERT;

/*============================================================================*
 *                       Global Variables
 *============================================================================*/


/*============================================================================*
 *                       Static Functions
 *============================================================================*/
static T_APP_RESULT fms_handle_write_control_char(T_FMS_WRITE_MSG *p_write);
static T_APP_RESULT fms_handle_write_data_char(T_FMS_WRITE_MSG *p_write);

static T_APP_RESULT fms_handle_write_char_cb(T_FMS_WRITE_MSG *p_write);
static T_APP_RESULT fms_handle_read_char_cb(uint8_t read_value_index);
static T_APP_RESULT fms_handle_indication_notification_char_cb(uint8_t
                                                               notification_indification_index);

static void fms_periodical_conn_callback(TimerHandle_t p_timer) DATA_RAM_FUNCTION;
/******************************************************************
 * @fn          fms_handle_write_char_cb
 * @brief      FMS service handle write char callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT fms_handle_write_char_cb(T_FMS_WRITE_MSG *p_write)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    uint8_t write_type = p_write->write_type;
    APP_PRINT_INFO1("[fms_handle_write_char_cb] write_type is %d", write_type);

    switch (write_type)
    {
    case FMS_WRITE_CHAR_CONTROL:
        {
            fms_handle_write_control_char(p_write);
        }
        break;
    case FMS_WRITE_CHAR_DATA:
        {
            fms_handle_write_data_char(p_write);
        }
        break;
    default:
        cb_result = APP_RESULT_APP_ERR;
        break;
    }


    return cb_result;
}

/******************************************************************
 * @fn          fms_handle_read_char_cb
 * @brief      FMS service handle read char callback.
 * @param    read_value_index  - read value index
 * @return     T_APP_RESULT
 */
T_APP_RESULT fms_handle_read_char_cb(uint8_t read_value_index)
{
    T_APP_RESULT cb_result = APP_RESULT_APP_ERR;

    APP_PRINT_INFO1("[fms_handle_read_char_cb] read_value_index is %d", read_value_index);

    return cb_result;
}

/******************************************************************
 * @fn          fms_handle_indication_notification_char_cb
 * @brief      FMS service handle indication notification char callback.
 * @param    notification_indification_index  - notification or indication index
 * @return     T_APP_RESULT
 */
T_APP_RESULT fms_handle_indication_notification_char_cb(uint8_t
                                                        notification_indification_index)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    APP_PRINT_INFO1("[fms_handle_indication_notification_char_cb] notification_indification_index is %d",
                    notification_indification_index);

    if (notification_indification_index == FMS_DATA_NOTIFY_ENABLE)
    {
        Fms_Handle_Notify_Flag = true;
    }
    else if (notification_indification_index == FMS_DATA_NOTIFY_DISABLE)
    {
        Fms_Handle_Notify_Flag = false;
    }


    return cb_result;
}

/******************************************************************
 * @fn          fms_handle_write_control_char
 * @brief      FMS service handle write GATT_SVC_IR_PROG_CONTROL_INDEX callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT fms_handle_write_control_char(T_FMS_WRITE_MSG *p_write)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    uint16_t data_len = p_write->write_parameter.report_data.len;
    uint8_t *p_data = p_write->write_parameter.report_data.report;

    if (data_len == FMS_WRITE_CHAR_CONTROL_LEN)
    {
        LE_ARRAY_TO_UINT16(Fms_Current_Mode, p_data);
        cb_result = APP_RESULT_SUCCESS;
    }
    else
    {
        cb_result = APP_RESULT_APP_ERR;
    }

    APP_PRINT_INFO2("[fms_handle_write_control_char] cb_result = %d, Fms_Current_Mode = 0x%04X",
                    cb_result, Fms_Current_Mode);

    return cb_result;
}

/******************************************************************
 * @fn          fms_handle_write_data_char
 * @brief      FMS service handle write GATT_SVC_IR_KEY_ID_INDEX callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT fms_handle_write_data_char(T_FMS_WRITE_MSG *p_write)
{
    uint16_t status = FMS_STATUS_OK;
    uint16_t error_code = FMS_ERROR_NO_ERR;
    uint16_t cmd_type;
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    uint16_t data_len = p_write->write_parameter.report_data.len;
    uint8_t *p_data = p_write->write_parameter.report_data.report;

    if (Fms_Current_Mode == FMS_CTL_MODE_ALERT)
    {
        if (data_len >= 2)
        {
            LE_ARRAY_TO_UINT16(cmd_type, p_data);

            APP_PRINT_INFO1("[fms_handle_write_data_char] FMS_CTL_MODE_ALERT cmd_type = 0x%04X", cmd_type);

            if (cmd_type == FMS_DATA_STOP_ALERT)
            {
                /* stop alert */
                buzzer_stop_pwm_output();
            }
            else if (cmd_type == FMS_DATA_ALERT_LEVEL_LOW)
            {
                /* low level alert */
                buzzer_start_pwm_output(1000, 25);
            }
            else if (cmd_type == FMS_DATA_ALERT_LEVEL_MEDIUM)
            {
                /* medium level alert */
                buzzer_start_pwm_output(1000, 50);
            }
            else if (cmd_type == FMS_DATA_ALERT_LEVEL_HIGH)
            {
                /* high level alert */
                buzzer_start_pwm_output(1000, 75);
            }
            else
            {
                status = FMS_STATUS_ERROR;
                error_code = FMS_ERROR_UNKNOWN_CMD;
            }
        }
        else
        {
            status = FMS_STATUS_ERROR;
            error_code = FMS_ERROR_UNKNOWN_CMD;
        }
    }
    else
    {
        status = FMS_STATUS_ERROR;
        error_code = FMS_ERROR_MODE_DISABLED;
    }

    fms_handle_notify_status(status, error_code);

    return cb_result;
}

/******************************************************************
 * @brief FMS periodical connection timer callback
 *
 * fms_periodical_conn_callback is used to start advertising after timeout
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void fms_periodical_conn_callback(TimerHandle_t p_timer)
{
    if (app_global_data.is_link_key_existed == true)
    {
        if (app_global_data.rcu_status == RCU_STATUS_IDLE)
        {
            app_global_data.wakeup_key_index = KEY_ID_NONE;
            rcu_start_adv(ADV_UNDIRECT_WAKEUP);
        }
        else
        {
            os_timer_start(&Fms_Periodical_Conn_Timer);
        }
    }
}

/*============================================================================*
 *                       Global Functions
 *============================================================================*/
/******************************************************************
 * @fn         fms_handle_init_data
 * @brief      Initialize FMS handler data.
 * @param      void
 * @return     void
 */
void fms_handle_init_data(void)
{
    APP_PRINT_INFO0("[fms_handle_init_data] init data");

    Fms_Handle_Notify_Flag = false;
    Fms_Current_Mode = FMS_CTL_MODE_ALERT;
}

/******************************************************************
 * @fn          fms_handle_srv_cb
 * @brief      FMS service callbacks are handled in this function.
 * @param    p_data  - pointer to callback data
 * @return     T_APP_RESULT
 */
T_APP_RESULT fms_handle_srv_cb(T_FMS_CALLBACK_DATA *p_data)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    APP_PRINT_INFO1("[fms_handle_srv_cb] msg_type: %d", p_data->msg_type);

    switch (p_data->msg_type)
    {
    case SERVICE_CALLBACK_TYPE_READ_CHAR_VALUE:
        cb_result = fms_handle_read_char_cb(p_data->msg_data.read_value_index);
        break;

    case SERVICE_CALLBACK_TYPE_WRITE_CHAR_VALUE:
        cb_result = fms_handle_write_char_cb(&p_data->msg_data.write_msg);
        break;

    case SERVICE_CALLBACK_TYPE_INDIFICATION_NOTIFICATION:
        cb_result = fms_handle_indication_notification_char_cb(
                        p_data->msg_data.notification_indification_index);
        break;

    default:
        APP_PRINT_INFO1("[fms_handle_srv_cb] unknown msg_type: %d", p_data->msg_type);
        cb_result = APP_RESULT_APP_ERR;
        break;
    }

    return cb_result;
}

/******************************************************************
 * @fn         fms_handle_disconnect_event
 * @brief      Handle disconnect event.
 * @param      void
 * @return     void
 */
void fms_handle_disconnect_event(void)
{
    Fms_Handle_Notify_Flag = false;

    APP_PRINT_INFO2("[fms_handle_disconnect_event] en_find_me = %d, find_me_wakeup_timeout = %d",
                    app_global_data.en_find_me, app_global_data.find_me_wakeup_timeout);

    /* check and start periodical connention timer */
    if ((app_global_data.en_find_me == 1)
        && (app_global_data.find_me_wakeup_timeout >= MIN_FMS_PERIODICAL_CONN_TIMEOUT)
        && (app_global_data.find_me_wakeup_timeout <= MAX_FMS_PERIODICAL_CONN_TIMEOUT))
    {
        os_timer_restart(&Fms_Periodical_Conn_Timer, app_global_data.find_me_wakeup_timeout * 60 * 1000);
    }
}

/******************************************************************
 * @fn         fms_handle_connect_event
 * @brief      Handle connect event.
 * @param      void
 * @return     void
 */
void fms_handle_connect_event(void)
{
    APP_PRINT_INFO0("[fms_handle_connect_event] connection");
    os_timer_stop(&Fms_Periodical_Conn_Timer);
}

/******************************************************************
 * @fn         fms_handle_get_current_mode
 * @brief      Get current FMS mode.
 * @param      void
 * @return     uint16_t - current mode
 */
uint16_t fms_handle_get_current_mode(void)
{
    return Fms_Current_Mode;
}

/******************************************************************
 * @fn         fms_handle_notify_status
 * @brief      FMS notify status.
 * @param      status, error_code
 * @return     bool - true or false
 */
bool fms_handle_notify_status(uint16_t status, uint16_t error_code)
{
    bool result = true;
    uint8_t notify_data[4];

    LE_UINT16_TO_ARRAY(&notify_data[0], status);
    LE_UINT16_TO_ARRAY(&notify_data[2], error_code);

    if ((app_global_data.rcu_status == RCU_STATUS_PAIRED)
        && (Fms_Handle_Notify_Flag == true))
    {
        result = server_send_data(0, app_global_data.fms_srv_id, GATT_SVC_FMS_DATA_INDEX, notify_data,
                                  sizeof(notify_data), GATT_PDU_TYPE_NOTIFICATION);
    }
    else
    {
        result = false;
    }

    APP_PRINT_INFO3("[fms_handle_notify_status] result = %d, status = 0x%04X, error_code = 0x%04X",
                    result, status, error_code);

    return result;
}

/******************************************************************
 * @brief    key handler init timer
 * @param    none
 * @return   none
 * @retval   void
 */
void fms_handle_init_timer(void)
{
    APP_PRINT_INFO0("[fms_handle_init_timer] init timer");

    /* fms_periodical_conn_callback is used to start advertising after timeout */
    if (false == os_timer_create(&Fms_Periodical_Conn_Timer, "Fms_Periodical_Conn_Timer",  1, \
                                 (DEFAULT_FMS_PERIODICAL_CONN_TIMEOUT * 60 * 1000), false, fms_periodical_conn_callback))
    {
        APP_PRINT_ERROR0("[fms_handle_init_timer] Fms_Periodical_Conn_Timer creat failed!");
    }
}

#endif
