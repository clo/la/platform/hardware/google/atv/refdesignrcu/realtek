/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file         ir_send_handle.c
* @brief        This file provides IR sending data handler by interrupt.
* @details      Application users can use the global functions to send IR data.
* @author       barry_bian
* @date         2020-02-26
* @version      v1.1
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "board.h"
#include "trace.h"
#include "string.h"
#include "rtl876x_ir.h"
#include "app_msg.h"
#include "ir_protocol_nec.h"
#include "ir_send_driver.h"
#include "ir_send_handle.h"
#include "ir_service_handle.h"
#include "key_handle.h"

#if SUPPORT_IR_TX_FEATURE

/*============================================================================*
 *                          Local Variables
 *============================================================================*/
static T_IR_SEND_KEY_STATE ir_send_key_state = IR_SEND_KEY_RELEASE;
static T_IR_SEND_PARA ir_send_parameters;

/*============================================================================*
 *                          Local Functions
 *============================================================================*/
/******************************************************************
* @brief   encode ir send command data
* @param   T_IR_PROTOCOL protocol
* @param   uint8_t ir_key_command
* @param   T_IR_SEND_PARA *p_ir_send_parameters
* @return  result
* @retval  T_IRDA_RET
*/
static T_IRDA_RET ir_send_command_encode(T_IR_PROTOCOL ir_protocol, IR_KEY_CODE ir_key_code,
                                         T_IR_SEND_PARA *p_ir_send_parameters)
{
    T_IRDA_RET ret = IRDA_SUCCEED;

    switch (ir_protocol)
    {
    case NEC_PROTOCOL:
        ret = ir_protocol_nec_command_encode(ir_key_code, p_ir_send_parameters);
        if (ret == IRDA_SUCCEED)
        {
            ret = ir_protocol_nec_repeat_code_encode(p_ir_send_parameters);
        }
        break;
    default:
        break;
    }

    return ret;
}

/******************************************************************
* @brief   encode ir send repeat code data
* @param   T_IR_PROTOCOL protocol
* @param   uint8_t ir_key_command
* @param   T_IR_SEND_PARA *p_ir_send_parameters
* @return  result
* @retval  T_IRDA_RET
*/
T_IRDA_RET ir_send_repeat_code_encode(T_IR_PROTOCOL ir_protocol, IR_KEY_CODE ir_key_code,
                                      T_IR_SEND_PARA *p_ir_send_parameters)
{
    T_IRDA_RET ret = IRDA_SUCCEED;

    switch (ir_protocol)
    {
    case NEC_PROTOCOL:
        ret = ir_protocol_nec_repeat_code_encode(p_ir_send_parameters);
        break;
    default:
        break;
    }

    return ret;
}

/******************************************************************
* @brief   ir_send_raw_encode
* @param   key_index
* @return  result
* @retval  true or false
*/
bool ir_send_raw_encode(T_KEY_INDEX_DEF key_index)
{
    bool result = false;
    uint32_t code_size = 0;
    uint8_t code[MAX_CODE_LENGTH];
    uint32_t data_index = 0;
    uint8_t duty_cycle = 0;
    uint16_t carrier_freq = 0;
    uint16_t seq_len_1 = 0;
    uint16_t seq_len_2 = 0;
    uint16_t repeat_delay;
    uint16_t on_data_value;
    uint16_t off_data_value;
    uint32_t total_cycle_cnt;

    if (true == ir_service_handle_get_key_code_by_key_id((uint8_t)key_index, &code_size, code))
    {
        /* get code type */
        ir_send_parameters.ir_send_type = code[data_index++];

        if (code_size >= 8)
        {
            data_index = 1;
            duty_cycle = code[data_index++];
            if (duty_cycle == 0)
            {
                duty_cycle = 50;
            }
            ir_send_parameters.duty_cycle = 100 / (float)duty_cycle;

            carrier_freq = ((uint16_t)code[data_index] << 8) + code[data_index + 1];
            data_index += 2;
            if (carrier_freq == 0)
            {
                carrier_freq = 380;
            }
            ir_send_parameters.carrier_frequency_hz = carrier_freq * 100;

            seq_len_1 = ((uint16_t)code[data_index] << 8) + code[data_index + 1];
            data_index += 2;

            seq_len_2 = ((uint16_t)code[data_index] << 8) + code[data_index + 1];
            data_index += 2;
            repeat_delay = seq_len_2;
        }

        if ((ir_send_parameters.ir_send_type == IR_SEND_TYPE_ONE_TIME)
            && (code_size >= seq_len_1 * 4 + 6))
        {
            data_index = 6;
            total_cycle_cnt = 0;

            for (uint8_t loop_index = 0; loop_index < seq_len_1; loop_index++)
            {
                on_data_value = ((uint16_t)code[data_index] << 8) + code[data_index + 1];
                off_data_value = ((uint16_t)code[data_index + 2] << 8) + code[data_index + 3];
                data_index += 4;
                total_cycle_cnt += on_data_value + off_data_value;

                ir_send_parameters.ir_seq_buf_1[2 * loop_index] = on_data_value | PULSE_HIGH;
                ir_send_parameters.ir_seq_buf_1[2 * loop_index + 1] = off_data_value;
            }
            ir_send_parameters.ir_seq_len_1 = seq_len_1 * 2;
            if (carrier_freq != 0)
            {
                ir_send_parameters.ir_seq_timeout_ms_1 = (total_cycle_cnt * 10 + (carrier_freq >> 1)) /
                                                         carrier_freq;
            }
            result = true;
        }
        else if ((ir_send_parameters.ir_send_type == IR_SEND_TYPE_REPEATED_SEQ)
                 && (code_size >= seq_len_1 * 4 + 8))
        {
            data_index = 8;
            total_cycle_cnt = 0;

            for (uint8_t loop_index = 0; loop_index < seq_len_1; loop_index++)
            {
                on_data_value = ((uint16_t)code[data_index] << 8) + code[data_index + 1];
                off_data_value = ((uint16_t)code[data_index + 2] << 8) + code[data_index + 3];
                data_index += 4;
                total_cycle_cnt += on_data_value + off_data_value;

                ir_send_parameters.ir_seq_buf_1[2 * loop_index] = on_data_value | PULSE_HIGH;
                ir_send_parameters.ir_seq_buf_1[2 * loop_index + 1] = off_data_value;
            }
            ir_send_parameters.ir_seq_len_1 = seq_len_1 * 2;
            ir_send_parameters.ir_seq_timeout_ms_1 = (repeat_delay * 10 + (carrier_freq >> 1)) / carrier_freq;

            result = true;
        }
        else if ((ir_send_parameters.ir_send_type == IR_SEND_TYPE_ONE_TIME_PLUS_REP_SEQ)
                 || (ir_send_parameters.ir_send_type == IR_SEND_TYPE_TWO_REP_SEQ))
        {
            if (code_size >= seq_len_1 * 4 + seq_len_2 * 4 + 8)
            {
                data_index = 8;

                total_cycle_cnt = 0;
                for (uint8_t loop_index = 0; loop_index < seq_len_1; loop_index++)
                {
                    on_data_value = ((uint16_t)code[data_index] << 8) + code[data_index + 1];
                    off_data_value = ((uint16_t)code[data_index + 2] << 8) + code[data_index + 3];
                    data_index += 4;
                    total_cycle_cnt += on_data_value + off_data_value;

                    ir_send_parameters.ir_seq_buf_1[2 * loop_index] = on_data_value | PULSE_HIGH;
                    ir_send_parameters.ir_seq_buf_1[2 * loop_index + 1] = off_data_value;
                }
                ir_send_parameters.ir_seq_len_1 = seq_len_1 * 2;
                ir_send_parameters.ir_seq_timeout_ms_1 = (total_cycle_cnt * 10 + (carrier_freq >> 1)) /
                                                         carrier_freq;

                total_cycle_cnt = 0;
                for (uint8_t loop_index = 0; loop_index < seq_len_2; loop_index++)
                {
                    on_data_value = ((uint16_t)code[data_index] << 8) + code[data_index + 1];
                    off_data_value = ((uint16_t)code[data_index + 2] << 8) + code[data_index + 3];
                    data_index += 4;
                    total_cycle_cnt += on_data_value + off_data_value;

                    ir_send_parameters.ir_seq_buf_2[2 * loop_index] = on_data_value | PULSE_HIGH;
                    ir_send_parameters.ir_seq_buf_2[2 * loop_index + 1] = off_data_value;
                }
                ir_send_parameters.ir_seq_len_2 = seq_len_2 * 2;
                ir_send_parameters.ir_seq_timeout_ms_2 = (total_cycle_cnt * 10 + (carrier_freq >> 1)) /
                                                         carrier_freq;

                result = true;
            }
        }
    }
    else
    {
        APP_PRINT_WARN0("[ir_send_key_press_handle] Failed to get key code");
    }

    APP_PRINT_INFO4("[ir_send_raw_encode] carrier_freq = %d, duty_cycle = %d, seq_len_1 = %d, seq_len_2 = %d",
                    carrier_freq, duty_cycle, seq_len_1, seq_len_2);
    APP_PRINT_INFO4("[ir_send_raw_encode] key_index = %d, result = %d, ir_send_type = %d, code_size = %d",
                    key_index,
                    result, ir_send_parameters.ir_send_type, code_size);

    return result;
}

/*============================================================================*
 *                          Global Functions
 *============================================================================*/
/******************************************************************
 * @brief   IR app send message process.
 * @param   msg_sub_type - message subtype
 * @return  none
 * @retval  void
 */
void ir_send_msg_proc(uint16_t msg_sub_type)
{
    APP_PRINT_INFO2("[ir_send_msg_proc] msg_sub_type is %d, ir_send_get_current_state is %d",
                    msg_sub_type, ir_send_get_current_state());

    if (ir_send_key_state == IR_SEND_KEY_RELEASE)
    {
        ir_send_exit();
        return;
    }

    if (msg_sub_type == IO_MSG_TYPE_IR_START_SEND_REPEAT_CODE)
    {
        if (ir_send_get_current_state() == IR_SEND_CAMMAND_COMPLETE ||
            ir_send_get_current_state() == IR_SEND_REPEAT_CODE_COMPLETE)
        {
            if (false == ir_send_repeat_code_start())
            {
                ir_send_exit();
            }
        }
        else
        {
            ir_send_exit();
        }
    }
}

/******************************************************************
 * @brief   Application code for IR send key press handle.
 * @param   T_KEY_INDEX_DEF ir_send_key_index
 * @return  none
 * @retval  void
 */
void ir_send_key_press_handle(T_KEY_INDEX_DEF ir_key_index, T_IR_PROTOCOL ir_protocol)
{
    bool result = false;
    ir_send_key_state = IR_SEND_KEY_PRESS;

    memset(&ir_send_parameters, 0, sizeof(T_IR_SEND_PARA));

    if (ir_protocol == NEC_PROTOCOL)
    {
        IR_KEY_CODE ir_code = key_handle_get_ir_key_code_by_index(ir_key_index);
        ir_send_command_encode(NEC_PROTOCOL, ir_code, &ir_send_parameters);
        ir_send_parameters.ir_send_type = IR_SEND_TYPE_ONE_TIME_PLUS_REP_SEQ;
        result = true;
    }
    else if (ir_protocol == RAW_PROTOCOL)
    {
        result = ir_send_raw_encode(ir_key_index);
    }
    else
    {
        APP_PRINT_WARN1("[ir_send_key_press_handle] Unknown ir_protocol is %d", ir_protocol);
        result = false;
    }


    if ((result == true)
        && (true == ir_send_module_init(&ir_send_parameters)))
    {
        result = ir_send_command_start();
    }

    APP_PRINT_INFO2("[IR] ir send key press, ir_protocol is %d, result is %d", ir_protocol, result);
}

/******************************************************************
 * @brief   Application code for IR send data one time handle.
 * @param   T_KEY_INDEX_DEF ir_send_key_index
 * @return  none
 * @retval  void
 */
void ir_send_data_one_time_handle(T_KEY_INDEX_DEF ir_key_index, T_IR_PROTOCOL ir_protocol)
{
    bool result = false;
    ir_send_key_state = IR_SEND_KEY_PRESS;

    memset(&ir_send_parameters, 0, sizeof(T_IR_SEND_PARA));

    if (ir_protocol == NEC_PROTOCOL)
    {
        IR_KEY_CODE ir_code = key_handle_get_ir_key_code_by_index(ir_key_index);
        ir_send_command_encode(NEC_PROTOCOL, ir_code, &ir_send_parameters);
        ir_send_parameters.ir_send_type = IR_SEND_TYPE_ONE_TIME;
        result = true;
    }
    else if (ir_protocol == RAW_PROTOCOL)
    {
        result = ir_send_raw_encode(ir_key_index);
    }
    else
    {
        APP_PRINT_WARN1("[ir_send_data_one_time_handle] Unknown ir_protocol is %d", ir_protocol);
        result = false;
    }


    if ((result == true)
        && (true == ir_send_module_init(&ir_send_parameters)))
    {
        result = ir_send_command_start();
    }

    APP_PRINT_INFO2("[IR] ir_send_data_one_time, ir_protocol is %d, result is %d", ir_protocol, result);
}
/******************************************************************
 * @brief   Application code for IR send key release handle.
 * @param   none
 * @return  none
 * @retval  void
 */
void ir_send_key_release_handle(void)
{
    APP_PRINT_INFO0("[IR] ir send key release.");
    ir_send_key_state = IR_SEND_KEY_RELEASE;

    if (ir_send_is_working() == false)
    {
        return;
    }

    if (ir_send_get_current_state() != IR_SEND_CAMMAND &&
        ir_send_get_current_state() != IR_SEND_REPEAT_CODE)
    {
        ir_send_exit();
    }
}

#endif /*end Micro @SUPPORT_IR_TX_FEATURE*/


/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/

