/**
*********************************************************************************************************
*               Copyright(c) 2018, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     ir_service_handle.c
* @brief    This is the entry of user code which the IR service handle module resides in.
* @details
* @author   chenjie jin
* @date     2022-03-22
* @version  v1.0
*********************************************************************************************************
*/

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include <string.h>
#include "board.h"
#include "ir_service_handle.h"
#include "ir_service.h"
#include "trace.h"
#include "profile_server.h"
#include "rcu_application.h"
#include "flash_map.h"
#include "os_timer.h"
#include "swtimer.h"
#include "os_mem.h"
#include "ftl.h"
#include "flash_device.h"
#include "key_handle.h"
#include "app_task.h"

#if FEAUTRE_SUPPORT_IR_OVER_BLE

/*============================================================================*
 *                       Static Variables
 *============================================================================*/
static uint32_t IR_Table_FTL_Addr_Info[MAX_PROG_KEY_COUNT] =
{FTL_IR_TABLE_SEC_0_ADDR, FTL_IR_TABLE_SEC_1_ADDR, FTL_IR_TABLE_SEC_2_ADDR, FTL_IR_TABLE_SEC_3_ADDR, FTL_IR_TABLE_SEC_4_ADDR};

static TimerHandle_t ir_programing_timer;

_attribute_data_retention_ app_nec_ir_table_sts nec_ir_table_start = APP_NEC_IR_TABLE_END;

//keyid: receive from the host. button_idx: Corresponding to the key index in the memory ir_table[].rcu_key_id:The actual keyid corresponding to the board
const key_button_pair_t key_button_map_g10[MAX_PROG_KEY_COUNT] =
{
    {0x0018, KEY_ID_VolUp},
    {0x0019, KEY_ID_VolDown},
    {0x00A4, KEY_ID_Mute},
    {0x001A, KEY_ID_Power},
    {0x00B2, KEY_ID_Input}
};

//keyid: receive from the host. button_idx: Corresponding to the key index in the memory ir_table[].rcu_key_id:The actual keyid corresponding to the board
const key_button_pair_t key_button_map_g20[MAX_PROG_KEY_COUNT] =
{
    {0x0018, KEY_ID_VolUp},
    {0x0019, KEY_ID_VolDown},
    {0x00A4,  KEY_ID_Mute},
    {0x001A, KEY_ID_Power},
    {0x00B2, KEY_ID_Input}
};

static _attribute_data_retention_ key_code_t ir_table[MAX_PROG_KEY_COUNT] = {{0}, {0}, {0}, {0}, {0}};

/*============================================================================*
 *                       Global Variables
 *============================================================================*/
T_IR_SVC_HANDLE_GLOBAL_DATA ir_svc_handle_global_data;

/*============================================================================*
 *                       Static Functions
 *============================================================================*/
static T_APP_RESULT ir_service_handle_write_prog_control_char(T_IR_WRITE_MSG *p_write);
static T_APP_RESULT ir_service_handle_write_key_id_char(T_IR_WRITE_MSG *p_write);
static T_APP_RESULT ir_service_handle_write_code_char(T_IR_WRITE_MSG *p_write);
static T_APP_RESULT ir_service_handle_write_suppress_char(T_IR_WRITE_MSG *p_write);

static T_APP_RESULT ir_service_handle_write_char_cb(T_IR_WRITE_MSG *p_write);
static T_APP_RESULT ir_service_handle_read_char_cb(uint8_t read_value_index);
static T_APP_RESULT ir_service_handle_indication_notification_char_cb(uint8_t
                                                                      notification_indification_index);

static void ir_service_handle_activate_ir_table(void);
static void ir_service_handle_back_up_ftl_table(void);
static void ir_service_handle_restore_ir_table_from_ftl(void);
static bool ir_service_handle_ir_table_add_item(uint16_t atv_key_code, uint16_t data_len,
                                                uint8_t *p_data);

/******************************************************************
 * @fn          ir_service_handle_write_char_cb
 * @brief      IR service handle write char callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_write_char_cb(T_IR_WRITE_MSG *p_write)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    uint8_t write_type = p_write->write_type;
    APP_PRINT_INFO1("[ir_service_handle_write_char_cb] write_type is %d", write_type);

    switch (write_type)
    {
    case IR_WRITE_PROG_CONTROL:
        {
            ir_service_handle_write_prog_control_char(p_write);
        }
        break;
    case IR_WRITE_KEY_ID:
        {
            ir_service_handle_write_key_id_char(p_write);
        }
        break;
    case IR_WRITE_CODE:
        {
            ir_service_handle_write_code_char(p_write);
        }
        break;
    case IR_WRITE_SUPPRESS:
        {
            ir_service_handle_write_suppress_char(p_write);
        }
        break;
    default:
        cb_result = APP_RESULT_APP_ERR;
        break;
    }


    return cb_result;
}

/******************************************************************
 * @fn          ir_service_handle_read_char_cb
 * @brief      IR service handle read char callback.
 * @param    read_value_index  - read value index
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_read_char_cb(uint8_t read_value_index)
{
    T_APP_RESULT cb_result = APP_RESULT_APP_ERR;

    APP_PRINT_INFO1("[ir_service_handle_read_char_cb] read_value_index is %d", read_value_index);

    return cb_result;
}

/******************************************************************
 * @fn          ir_service_handle_indication_notification_char_cb
 * @brief      IR service handle indication notification char callback.
 * @param    notification_indification_index  - notification or indication index
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_indication_notification_char_cb(uint8_t
                                                               notification_indification_index)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    APP_PRINT_INFO1("[ir_service_handle_indication_notification_char_cb] notification_indification_index is %d",
                    notification_indification_index);

    if (notification_indification_index == IR_KEY_EVNET_NOTIFY_ENABLE)
    {
        ir_svc_handle_global_data.key_notification_flag = 1;
    }
    else if (notification_indification_index == IR_KEY_EVNET_NOTIFY_DISABLE)
    {
        ir_svc_handle_global_data.key_notification_flag = 0;
    }


    return cb_result;
}

/******************************************************************
 * @fn          ir_service_handle_write_prog_control_char
 * @brief      IR service handle write GATT_SVC_IR_PROG_CONTROL_INDEX callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_write_prog_control_char(T_IR_WRITE_MSG *p_write)
{
    uint32_t error_code = 0;
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;
    uint16_t data_len = p_write->write_parameter.report_data.len;
    uint8_t *p_data = p_write->write_parameter.report_data.report;

    if (data_len == 1)
    {
        ir_svc_handle_global_data.programming_start = *p_data;
        if (ir_svc_handle_global_data.programming_start == 1)
        {
            app_set_latency_status(LATENCY_IR_BLE_PROC_BIT, LANTENCY_OFF);
            ir_svc_handle_global_data.programmed_key_count = 0;
            ir_svc_handle_global_data.current_programming_key_id = INVALID_ATV_KEY_CODE;
            ir_service_handle_reset_ir_table();

            if (ir_programing_timer)
            {
                os_timer_restart(&ir_programing_timer, PROGRAMMING_TIMEOUT);
            }
        }
        else
        {
            APP_PRINT_INFO1("[ir_service_handle_write_prog_control_char] ir table programming end, key_count = %d",
                            ir_svc_handle_global_data.programmed_key_count);

            app_set_latency_status(LATENCY_IR_BLE_PROC_BIT, LANTENCY_ON);
            if (ir_svc_handle_global_data.programmed_key_count > 0)
            {
                ir_service_handle_activate_ir_table();

                /* send message to APP task for FTL back-up */
                T_IO_MSG app_msg;
                app_msg.type = IO_MSG_TYPE_IR_FLASH;
                if (false == app_send_msg_to_apptask(&app_msg))
                {
                    APP_PRINT_INFO0("[ir_service_handle_write_prog_control_char] send IO_MSG_IR_FLASH_SAVE failed");
                    cb_result = APP_RESULT_APP_ERR;
                }
            }

            if (ir_programing_timer)
            {
                os_timer_stop(&ir_programing_timer);
            }
        }
    }
    else
    {
        cb_result = APP_RESULT_APP_ERR;
    }

    APP_PRINT_INFO3("[ir_service_handle_write_prog_control_char] data_len is %d, result is %d, error_code is %d",
                    data_len, cb_result, error_code);

    return cb_result;
}

/******************************************************************
 * @fn          ir_service_handle_write_key_id_char
 * @brief      IR service handle write GATT_SVC_IR_KEY_ID_INDEX callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_write_key_id_char(T_IR_WRITE_MSG *p_write)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;
    uint16_t data_len = p_write->write_parameter.report_data.len;
    uint8_t *p_data = p_write->write_parameter.report_data.report;

    if ((ir_svc_handle_global_data.programming_start == 1) && (data_len == 2))
    {
        ir_svc_handle_global_data.current_programming_key_id = ((uint16_t)p_data[0] << 8) + p_data[1];

        if (ir_programing_timer)
        {
            os_timer_restart(&ir_programing_timer, PROGRAMMING_TIMEOUT);
        }
    }
    else
    {
        cb_result = APP_RESULT_APP_ERR;
    }

    APP_PRINT_INFO3("[ir_service_handle_write_key_id_char] data_len is %d, result is %d, atv_key_code is %d",
                    data_len, cb_result, ir_svc_handle_global_data.current_programming_key_id);

    return cb_result;
}

/******************************************************************
 * @fn          ir_service_handle_write_code_char
 * @brief      IR service handle write GATT_SVC_IR_CODE_INDEX callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_write_code_char(T_IR_WRITE_MSG *p_write)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;
    uint16_t data_len = p_write->write_parameter.report_data.len;
    uint8_t *p_data = p_write->write_parameter.report_data.report;

    if ((ir_svc_handle_global_data.programming_start == 1) && (data_len > 0))
    {
        if (ir_svc_handle_global_data.current_programming_key_id != INVALID_ATV_KEY_CODE)
        {
            if (true == ir_service_handle_ir_table_add_item(
                    ir_svc_handle_global_data.current_programming_key_id, data_len, p_data))
            {
                cb_result = APP_RESULT_SUCCESS;
            }
            else
            {
                cb_result = APP_RESULT_APP_ERR;
            }

            ir_svc_handle_global_data.current_programming_key_id = INVALID_ATV_KEY_CODE;
        }
        else
        {
            cb_result = APP_RESULT_APP_ERR;
        }

        if (ir_programing_timer)
        {
            os_timer_restart(&ir_programing_timer, PROGRAMMING_TIMEOUT);
        }
    }
    else
    {
        cb_result = APP_RESULT_APP_ERR;
    }

    APP_PRINT_INFO2("[ir_service_handle_write_code_char] data_len is %d, result is %d",
                    data_len, cb_result);

    return cb_result;
}

/******************************************************************
 * @fn          ir_service_handle_write_suppress_char
 * @brief      IR service handle write GATT_SVC_IR_SUPPRESS_INDEX callback.
 * @param    p_write  - pointer to write data
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_write_suppress_char(T_IR_WRITE_MSG *p_write)
{
    uint32_t error_code = 0;
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;
    uint16_t data_len = p_write->write_parameter.report_data.len;
    uint8_t *p_data = p_write->write_parameter.report_data.report;

    if (data_len == 2)
    {
        ir_svc_handle_global_data.current_programming_key_id = ((uint16_t)p_data[0] << 8) + p_data[1];

        uint8_t index = ir_service_handle_get_item_id_from_atv_id(
                            ir_svc_handle_global_data.current_programming_key_id);

        if (index < MAX_PROG_KEY_COUNT)
        {
            if (ir_svc_handle_global_data.ir_suppress_table[index])
            {
                ir_svc_handle_global_data.ir_suppress_table[index] = 0;
            }
            else
            {
                ir_svc_handle_global_data.ir_suppress_table[index] = 1;
            }
        }
        ir_svc_handle_global_data.current_programming_key_id = INVALID_ATV_KEY_CODE;
    }
    else
    {
        cb_result = APP_RESULT_APP_ERR;
    }

    APP_PRINT_INFO3("[ir_service_handle_write_suppress_char] data_len is %d, result is %d, error_code is %d",
                    data_len, cb_result, error_code);

    return cb_result;
}

/******************************************************************
 * @brief ir programming timer callback
 *
 * ir_programming_timeoutcb is used to handle ir programming
 *
 * @param p_timer - timer handler
 * @return none
 * @retval void
 */
void ir_programming_timeoutcb(TimerHandle_t p_timer)
{
    APP_PRINT_INFO0("[ir_programming_timeoutcb] timeout");

    ir_svc_handle_global_data.programming_start = 0;

    if (app_global_data.rcu_status == RCU_STATUS_PAIRED)
    {
        app_set_latency_status(LATENCY_IR_BLE_PROC_BIT, LANTENCY_ON);
    }
}

/******************************************************************
 * @fn         ir_service_handle_reset_ir_table
 * @brief      Initialize ir programming table data.
 * @param      void
 * @return     void
 */
void ir_service_handle_reset_ir_table(void)
{
    APP_PRINT_INFO0("[ir_service_handle_reset_ir_table] reset IR table");

    memset(ir_table, 0, sizeof(key_code_t) * MAX_PROG_KEY_COUNT);
    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        ir_table[index].atv_key_code = INVALID_ATV_KEY_CODE;
        key_handle_restore_key_type_id(ir_svc_handle_global_data.p_key_button_pair_table[index].rcu_key_id);
    }

    ir_service_handle_invalid_ftl_table();
}

/******************************************************************
 * @fn         ir_service_handle_active_ir_table
 * @brief      Activate ir programming table data.
 * @param      void
 * @return     void
 */
void ir_service_handle_activate_ir_table(void)
{
    APP_PRINT_INFO0("[ir_service_handle_activate_ir_table] Activate IR table");

    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        if (ir_table[index].atv_key_code != INVALID_ATV_KEY_CODE)
        {
            key_handle_set_key_type_prog_by_id(
                ir_svc_handle_global_data.p_key_button_pair_table[index].rcu_key_id);
        }
    }
}

/******************************************************************
 * @fn         ir_service_handle_active_ir_table
 * @brief      Activate ir programming table data.
 * @param      void
 * @return     void
 */
bool ir_service_handle_ir_table_add_item(uint16_t atv_key_code, uint16_t data_len, uint8_t *p_data)
{
    bool result = true;

    uint8_t table_item_id = ir_service_handle_get_item_id_from_atv_id(atv_key_code);

    if ((table_item_id < MAX_PROG_KEY_COUNT)
        && (ir_svc_handle_global_data.programmed_key_count < MAX_PROG_KEY_COUNT)
        && (data_len <= MAX_CODE_LENGTH))
    {
        ir_table[ir_svc_handle_global_data.programmed_key_count].code_size = data_len;
        memcpy(ir_table[ir_svc_handle_global_data.programmed_key_count].code, p_data, data_len);
        ir_table[ir_svc_handle_global_data.programmed_key_count].rcu_key_id =
            ir_svc_handle_global_data.p_key_button_pair_table[table_item_id].rcu_key_id;
        ir_table[ir_svc_handle_global_data.programmed_key_count].atv_key_code = atv_key_code;
        ir_svc_handle_global_data.programmed_key_count++;

        result = true;
    }
    else
    {
        result = false;
    }

    APP_PRINT_INFO3("[ir_service_handle_ir_table_add_item] result = %d, atv_key_code = 0x%004X, programmed_key_count = %d",
                    result, atv_key_code, ir_svc_handle_global_data.programmed_key_count);

    return result;
}

/******************************************************************
 * @fn         ir_service_handle_invalid_ftl_table
 * @brief      Invalid ir table in FTL.
 * @param      void
 * @return     void
 */
void ir_service_handle_invalid_ftl_table(void)
{
    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        /* check FTL IR table header */
        ir_table_ftl_hd_t item_hd;
        memset(&item_hd, 0xFF, sizeof(item_hd));

        if (0 == ftl_load(&item_hd, IR_Table_FTL_Addr_Info[index], sizeof(item_hd)))
        {
            if (item_hd.header == APP_IR_DATA_HEAD)
            {
                memset(&item_hd, 0xFF, sizeof(item_hd));
                if (0 != ftl_save(&item_hd, IR_Table_FTL_Addr_Info[index], sizeof(item_hd)))
                {
                    APP_PRINT_WARN0("[ir_service_handle_invalid_ftl_table] ftl_save failed");
                }
            }
        }
    }
}

/******************************************************************
 * @fn         ir_service_handle_back_up_ftl_table
 * @brief      Back up ir table into FTL.
 * @param      void
 * @return     void
 */
void ir_service_handle_back_up_ftl_table(void)
{
    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        uint16_t save_data_len = 0;
        bool result = false;

        if (ir_table[index].atv_key_code != INVALID_ATV_KEY_CODE)
        {
            save_data_len = ir_table[index].code_size + 8;
            if (save_data_len & 0x0003)
            {
                /* 4-bytes alignment adjustment */
                save_data_len = (save_data_len + 4) & 0xFFFC;
            }

            if (save_data_len <= FTL_IR_TABLE_ITEM_MAX_LEN - FTL_IR_TABLE_ITEM_HEADER_LEN)
            {
                if (0 == ftl_save(&ir_table[index], IR_Table_FTL_Addr_Info[index] + FTL_IR_TABLE_ITEM_HEADER_LEN,
                                  save_data_len))
                {
                    ir_table_ftl_hd_t ftl_hd_data;
                    ftl_hd_data.header = APP_IR_DATA_HEAD;
                    ftl_hd_data.data_len = save_data_len;

                    if (0 == ftl_save(&ftl_hd_data, IR_Table_FTL_Addr_Info[index], FTL_IR_TABLE_ITEM_HEADER_LEN))
                    {
                        result = true;
                        APP_PRINT_INFO2("[ir_service_handle_back_up_ftl_table] Save successful %d, %04X", index,
                                        ir_table[index].atv_key_code);
                    }
                }
            }
            else
            {
                result = false;
            }
        }

        APP_PRINT_INFO3("[ir_service_handle_back_up_ftl_table] result = %d, atv_key_code = 0x%04X, data_len = %d",
                        result, ir_table[index].atv_key_code, save_data_len);
    }

}

/******************************************************************
 * @fn         ir_service_handle_restore_ir_table_from_ftl
 * @brief      Restore ir table from FTL data.
 * @param      void
 * @return     void
 */
void ir_service_handle_restore_ir_table_from_ftl(void)
{
    uint32_t ftl_res = 0;
    ir_table_ftl_hd_t ftl_hd_data;

    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        bool result = false;

        memset(&ftl_hd_data, 0, sizeof(ftl_hd_data));
        ftl_res = ftl_load(&ftl_hd_data, IR_Table_FTL_Addr_Info[index], FTL_IR_TABLE_ITEM_HEADER_LEN);
        if (0 == ftl_res)
        {
            if ((ftl_hd_data.header == APP_IR_DATA_HEAD) &&
                (ftl_hd_data.data_len <= FTL_IR_TABLE_ITEM_MAX_LEN - FTL_IR_TABLE_ITEM_HEADER_LEN))
            {
                ftl_res = ftl_load(&ir_table[index], IR_Table_FTL_Addr_Info[index] + FTL_IR_TABLE_ITEM_HEADER_LEN,
                                   8);
                if (0 == ftl_res)
                {
                    uint16_t load_size = ir_table[index].code_size;
                    if (load_size & 0x0003)
                    {
                        /* 4-bytes alignment adjustment */
                        load_size = (load_size + 4) & 0xFFFC;
                    }

                    if ((load_size > 0) && (load_size <= MAX_CODE_LENGTH))
                    {
                        ftl_res = ftl_load(&ir_table[index].code,
                                           IR_Table_FTL_Addr_Info[index] + FTL_IR_TABLE_ITEM_HEADER_LEN + 8, load_size);
                        if (0 == ftl_res)
                        {
                            result = true;
                        }
                        else
                        {
                            APP_PRINT_WARN2("[ir_service_handle_restore_ir_table_from_ftl] ftl_load failed_2, addr = 0x%X, len = %d",
                                            IR_Table_FTL_Addr_Info[index], load_size);
                        }
                    }
                }
                else
                {
                    APP_PRINT_WARN1("[ir_service_handle_restore_ir_table_from_ftl] ftl_load failed, addr = 0x%X",
                                    IR_Table_FTL_Addr_Info[index]);
                }
            }
            else
            {
                APP_PRINT_WARN2("[ir_service_handle_restore_ir_table_from_ftl] Invalid header or length, 0x%04X, %d",
                                ftl_hd_data.header, ftl_hd_data.data_len);
            }
        }

        if (result == false)
        {
            ir_table[index].atv_key_code = INVALID_ATV_KEY_CODE;
        }

        APP_PRINT_INFO3("[ir_service_handle_restore_ir_table_from_ftl] index = %d, result = %d, ftl_res = %d",
                        index,
                        result, ftl_res);
    }
}

/*============================================================================*
 *                       Global Functions
 *============================================================================*/
/******************************************************************
 * @fn         ir_service_handle_init_data
 * @brief      Initialize key programming handler data.
 * @param      void
 * @return     void
 */
void ir_service_handle_init_data(void)
{
    APP_PRINT_INFO0("[ir_service_handle_init_data] init data");

    memset(&ir_svc_handle_global_data, 0, sizeof(ir_svc_handle_global_data));
    ir_svc_handle_global_data.current_programming_key_id = INVALID_ATV_KEY_CODE;
    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        ir_svc_handle_global_data.ir_suppress_table[index] = INVALID_ATV_KEY_CODE;
    }

    if (app_global_data.device_type == REMOTE_G10)
    {
        ir_svc_handle_global_data.p_key_button_pair_table = key_button_map_g10;
    }
    else
    {
        ir_svc_handle_global_data.p_key_button_pair_table = key_button_map_g20;
    }

    ir_service_handle_restore_ir_table_from_ftl();
    ir_service_handle_activate_ir_table();

    if (ir_programing_timer == NULL)
    {
        if (false == os_timer_create(&ir_programing_timer, "ir_programing_timer", 1,
                                     PROGRAMMING_TIMEOUT, false, ir_programming_timeoutcb))
        {
            APP_PRINT_INFO0("[ir_service_handle_init_data] create ir_programing_timer failed!");
        }
    }
}

/******************************************************************
 * @fn         ir_service_handle_get_item_id_from_atv_id
 * @brief      Get table_item_id from atv_key_code
 * @param      atv_key_code
 * @return     table_item_id
 */
uint8_t ir_service_handle_get_item_id_from_atv_id(uint16_t atv_key_code)
{
    uint8_t table_item_id = INVALID_TABLE_ITEM_ID;

    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        if (atv_key_code == ir_svc_handle_global_data.p_key_button_pair_table[index].atv_key_code)
        {
            table_item_id = index;
        }
    }

    return table_item_id;
}

/******************************************************************
 * @fn         ir_service_handle_get_item_id_from_key_id
 * @brief      Get table_item_id from rcu_key_id
 * @param      atv_key_code
 * @return     table_item_id
 */
uint8_t ir_service_handle_get_item_id_from_key_id(uint16_t rcu_key_id)
{
    uint8_t table_item_id = INVALID_TABLE_ITEM_ID;

    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        if (rcu_key_id == ir_svc_handle_global_data.p_key_button_pair_table[index].rcu_key_id)
        {
            table_item_id = index;
        }
    }

    return table_item_id;
}

/******************************************************************
 * @fn          ir_service_handle_srv_cb
 * @brief      IR service callbacks are handled in this function.
 * @param    p_data  - pointer to callback data
 * @return     T_APP_RESULT
 */
T_APP_RESULT ir_service_handle_srv_cb(T_IR_CALLBACK_DATA *p_data)
{
    T_APP_RESULT cb_result = APP_RESULT_SUCCESS;

    APP_PRINT_INFO1("[ir_service_handle_srv_cb] msg_type: %d", p_data->msg_type);

    switch (p_data->msg_type)
    {
    case SERVICE_CALLBACK_TYPE_READ_CHAR_VALUE:
        cb_result = ir_service_handle_read_char_cb(p_data->msg_data.read_value_index);
        break;

    case SERVICE_CALLBACK_TYPE_WRITE_CHAR_VALUE:
        cb_result = ir_service_handle_write_char_cb(&p_data->msg_data.write_msg);
        break;

    case SERVICE_CALLBACK_TYPE_INDIFICATION_NOTIFICATION:
        cb_result = ir_service_handle_indication_notification_char_cb(
                        p_data->msg_data.notification_indification_index);
        break;

    default:
        APP_PRINT_INFO1("[ir_service_handle_srv_cb] unknown msg_type: %d", p_data->msg_type);
        cb_result = APP_RESULT_APP_ERR;
        break;
    }

    return cb_result;
}

/******************************************************************
 * @fn         ir_service_handle_disconnect_event
 * @brief      Handle disconnect event.
 * @param      void
 * @return     void
 */
void ir_service_handle_disconnect_event(void)
{
    APP_PRINT_INFO0("[ir_service_handle_disconnect_event] disconnection");

    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        ir_svc_handle_global_data.ir_suppress_table[index] = INVALID_ATV_KEY_CODE;
    }
}

/******************************************************************
 * @fn         ir_service_handle_flash_save_event
 * @brief      Handle flash save event.
 * @param      void
 * @return     void
 */
void ir_service_handle_flash_save_event(void)
{
    ir_service_handle_back_up_ftl_table();
}

/******************************************************************
 * @fn         ir_service_handle_is_key_suppressed
 * @brief      Chech whether key is suppressed or not
 * @param      bool
 * @return     true or false
 */
bool ir_service_handle_is_key_suppressed(uint8_t key_id)
{
    bool result = false;
    uint8_t table_item_id = ir_service_handle_get_item_id_from_key_id(key_id);
    uint16_t atv_key_code =
        ir_svc_handle_global_data.p_key_button_pair_table[table_item_id].atv_key_code;

    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        if (atv_key_code == ir_svc_handle_global_data.ir_suppress_table[index])
        {
            result = true;
            break;
        }
    }

    APP_PRINT_INFO2("[ir_service_handle_is_key_suppressed] key id = %d, result = %d", key_id, result);

    return result;
}

/******************************************************************
 * @fn         ir_service_handle_is_key_suppressed
 * @brief      Chech whether key is suppressed or not
 * @param      bool
 * @return     true or false
 */
bool ir_service_handle_get_key_code_by_key_id(uint8_t key_id, uint32_t *p_code_size,
                                              uint8_t *p_key_code)
{
    bool result = false;

    for (uint8_t index = 0; index < MAX_PROG_KEY_COUNT; index++)
    {
        if ((ir_table[index].atv_key_code != INVALID_ATV_KEY_CODE)
            && (ir_table[index].rcu_key_id == key_id)
            && (ir_table[index].code_size > 0)
            && (ir_table[index].code_size < MAX_CODE_LENGTH))
        {
            *p_code_size = ir_table[index].code_size;
            memcpy(p_key_code, ir_table[index].code, *p_code_size);
            result = true;
            break;
        }
    }

    APP_PRINT_INFO2("[ir_service_handle_get_key_code_by_key_id] key id = %d, result = %d", key_id,
                    result);

    return result;
}

/******************************************************************
 * @fn         ir_service_handle_notify_ir_key_event
 * @brief      Notify IR key event
 * @param      key_id - key index
 * @param      action_type - key action type
 * @return     true or false
 */
bool ir_service_handle_notify_ir_key_event(uint8_t key_id, uint8_t action_type)
{
    bool result = false;
    uint8_t data[3];

    uint8_t item_id = ir_service_handle_get_item_id_from_key_id(key_id);
    if ((INVALID_TABLE_ITEM_ID != item_id)
        && ((action_type == KEY_ACTION_DOWN) || (action_type == KEY_ACTION_UP)))
    {
        uint16_t atv_key_code = ir_svc_handle_global_data.p_key_button_pair_table[item_id].atv_key_code;

        data[0] = action_type;
        data[1] = (uint8_t)(atv_key_code >> 8);
        data[2] = (uint8_t)atv_key_code;

        result = server_send_data(0, app_global_data.ir_srv_id, GATT_SVC_IR_KEY_EVENT_INDEX, data,
                                  sizeof(data), GATT_PDU_TYPE_NOTIFICATION);
    }

    APP_PRINT_INFO3("[ir_service_handle_notify_ir_key_event] key_id = %d, action_type = %d, result = %d",
                    key_id, action_type, result);
    return result;
}

#endif
