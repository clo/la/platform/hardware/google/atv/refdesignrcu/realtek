/*
 *  Routines to access hardware
 *
 *  Copyright (c) 2019 Realtek Semiconductor Corp.
 *
 *  This module is a confidential and proprietary property of RealTek and
 *  possession or use of this module requires written permission of RealTek.
 */

#ifndef _IR_SERVICE_HANDLE_H_
#define _IR_SERVICE_HANDLE_H_

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include "ir_service.h"
#include "profile_server.h"
#include "swtimer.h"
#include "flash_map.h"
#include "frm_define.h"

/*============================================================================*
 *                              Macro Definitions
 *============================================================================*/
//programming timeout 30s
#define PROGRAMMING_TIMEOUT 30000

#define INVALID_ATV_KEY_CODE      0xFF00
#define MAX_CODE_LENGTH     500            /*  The maximum number of ir codes for each key. */
#define MAX_PROG_KEY_COUNT       5              /*  The maximum number of key for ir programming.*/

#define INVALID_TABLE_ITEM_ID   0xFF
#define KEY_IDX_NULL        0xff

#define APP_IR_CMD_OTA_NEC_IR_TABLE_PREPARE      0xEF00
#define APP_IR_CMD_OTA_NEC_IR_TABLE_START        0xEF01
#define APP_IR_CMD_OTA_NEC_IR_TABLE_END          0xEF02

#define APP_IR_DATA_HEAD          0xA55A

/*============================================================================*
 *                         Types
 *============================================================================*/

typedef struct
{
    u16 atv_key_code;                //Key ID, comes from ATV (host keyid)
    u16 rcu_key_id;            //button index, based on enum in firmware
    u32 code_size;
    u8  code[MAX_CODE_LENGTH];
} key_code_t;

typedef struct
{
    uint16_t header;
    uint16_t data_len;
} ir_table_ftl_hd_t;

typedef struct
{
    u32 release_timer;
    u32 repeat_delay;
} ir_repeat_delay_t;

typedef struct
{
    u8 programming_start;
    u8 programmed_key_count;
    u8 key_notification_flag;
    u16 current_programming_key_id;
} ir_programming_key_t;

typedef struct
{
    u16 atv_key_code;
    u8  rcu_key_id;
} key_button_pair_t;

typedef enum
{
    APP_NEC_IR_TABLE_PREPARE = 1,
    APP_NEC_IR_TABLE_PREPARE_END,
    APP_NEC_IR_TABLE_START,
    APP_NEC_IR_TABLE_WRITEFAIL,
    APP_NEC_IR_TABLE_END
} app_nec_ir_table_sts;

/**
 * @brief  ir service handle global data struct definition.
 */
typedef struct
{
    u8 programming_start;
    u8 programmed_key_count;
    u8 key_notification_flag;
    u16 current_programming_key_id;
    const key_button_pair_t *p_key_button_pair_table;
    u16 ir_suppress_table[MAX_PROG_KEY_COUNT];
} T_IR_SVC_HANDLE_GLOBAL_DATA;

/*============================================================================*
*                        Export Global Variables
*============================================================================*/
extern T_IR_SVC_HANDLE_GLOBAL_DATA ir_svc_handle_global_data;

/*============================================================================*
 *                         Functions
 *============================================================================*/
void ir_service_handle_init_data(void);
T_APP_RESULT ir_service_handle_srv_cb(T_IR_CALLBACK_DATA *p_data);
void ir_table_init(void);
void ir_service_handle_flash_save_event(void);
void ir_service_handle_disconnect_event(void);
uint8_t ir_service_handle_get_item_id_from_key_id(uint16_t rcu_key_id);
uint8_t ir_service_handle_get_item_id_from_atv_id(uint16_t atv_key_code);
bool ir_service_handle_is_key_suppressed(uint8_t key_id);
bool ir_service_handle_get_key_code_by_key_id(uint8_t key_id, uint32_t *p_code_size,
                                              uint8_t *p_key_code);
bool ir_service_handle_notify_ir_key_event(uint8_t key_id, uint8_t action_type);
void ir_service_handle_invalid_ftl_table(void);
void ir_service_handle_reset_ir_table(void);
#ifdef __cplusplus
}
#endif

#endif
