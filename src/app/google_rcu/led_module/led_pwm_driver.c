/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     led_pwm_driver.c
* @brief    this file provides led module function driver for users.
* @details  multi prio, multi event, multi led support.
* @author   Yuyin_zhang
* @date     2020-03-03
* @version  v1.1
* @note     the more led number and led event, the more time cpu spend on tick handle
*           For example: led_num = 8, led_event_num = 10, max time is 112us.
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include "board.h"
#include "trace.h"
#include "rtl876x_gpio.h"
#include "rtl876x_pinmux.h"
#include "swtimer.h"
#include "led_pwm_driver.h"
#include "rtl876x_tim.h"
#include "rtl876x_rcc.h"
#include "rcu_application.h"

#if SUPPORT_LED_INDICATION_FEATURE
/*============================================================================*
 *                              Local Variables
 *============================================================================*/
static uint8_t led_pwm_init_states = 0;
static uint8_t led_on_off_states = 0;

static const T_LED_PWM_PARAM_DEF led_pwm_list[LED_NUM_MAX] =
{
    {LED_R_PIN,    TIM_PWM2,     TIM2},
    {LED_G_PIN,    TIM_PWM3,     TIM3},
};

/*============================================================================*
 *                              Local Functions
 *============================================================================*/
/**
 * @brief  led module update pwm init states.
 * @param   index - led index
 * @param   state - ENABLE or DISABLE
 * @return  uint8_t - led_pwm_init_states
**/
static uint8_t led_module_update_pwm_states(uint8_t index, FunctionalState state)
{
    if (state == DISABLE)
    {
        led_pwm_init_states &= ~(1 << index);
    }
    else
    {
        led_pwm_init_states |= (1 << index);
    }

    return led_pwm_init_states;
}

/**
 * @brief  led module update led on/off states.
 * @param   index - led index
 * @param   state - ENABLE or DISABLE
 * @return  uint8_t - led_on_off_states
**/
static uint8_t led_module_update_on_off_states(uint8_t index, FunctionalState state)
{
    if (state == DISABLE)
    {
        led_on_off_states &= ~(1 << index);
    }
    else
    {
        led_on_off_states |= (1 << index);
    }

    return led_on_off_states;
}

/*============================================================================*
 *                              Global Functions
 *============================================================================*/
/******************************************************************
 * @brief   led module init.
 * @param   none
 * @return  none
 * @retval  void
 */
void led_pwm_init(void)
{
    led_pwm_init_states = 0;
    led_on_off_states = 0;

    led_driver_disable_boost_power();

    for (uint8_t index = 0; index < LED_NUM_MAX; index++)
    {
        led_module_pwm_stop(index);
    }
}

/******************************************************************
 * @brief    led driver check DLPS callback
 * @param    none
 * @return   bool
 * @retval   true or false
 */
bool led_pwm_driver_dlps_check(void)
{
    return (led_pwm_init_states == 0);
}

/******************************************************************
 * @brief   this function enable RGB LED boost power.
 */
void led_driver_enable_boost_power(void)
{
#if LED_PWM_DEBUG
    APP_PRINT_INFO0("[led_driver_enable_boost_power] enable LED power pin");
#endif
#ifdef LED_EN_PIN
    Pad_Config(LED_EN_PIN, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE, PAD_OUT_HIGH);
#endif
}

/******************************************************************
 * @brief   this function disable RGB LED boost power.
 */
void led_driver_disable_boost_power(void)
{
#if LED_PWM_DEBUG
    APP_PRINT_INFO0("[led_driver_disable_boost_power] disable LED power pin");
#endif
#ifdef LED_EN_PIN
    Pad_Config(LED_EN_PIN, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE, PAD_OUT_LOW);
#endif
}

/**
 * @brief  led module PWM start.
 * @param   index - led index
 * @return  void.
**/
void led_module_pwm_start(uint8_t index, uint32_t pwm_high_cnt, uint32_t pwm_low_cnt)
{
#if LED_PWM_DEBUG
    APP_PRINT_INFO3("[led_module_pwm_start] index = %d, pwm_high_cnt = 0x%X, pwm_low_cnt = 0x%X",
                    index, pwm_high_cnt, pwm_low_cnt);
#endif

    if (led_on_off_states == 0)
    {
        led_driver_enable_boost_power();
    }
    led_module_update_on_off_states(index, ENABLE);

    if (pwm_low_cnt == 0)
    {
#if (LED_ACTIVE_LEVEL == 1)
        Pad_Config(led_pwm_list[index].pin_num, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE,
                   PAD_OUT_HIGH);
#else
        Pad_Config(led_pwm_list[index].pin_num, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE,
                   PAD_OUT_LOW);
#endif
        led_module_update_pwm_states(index, DISABLE);
    }
    else if (pwm_high_cnt == 0)
    {
#if (LED_ACTIVE_LEVEL == 1)
        Pad_Config(led_pwm_list[index].pin_num, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE,
                   PAD_OUT_LOW);
#else
        Pad_Config(led_pwm_list[index].pin_num, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE,
                   PAD_OUT_HIGH);
#endif
        led_module_update_pwm_states(index, DISABLE);
    }
    else if ((led_pwm_init_states & (0x1 << index)) != 0)
    {
        TIM_PWMChangeFreqAndDuty(led_pwm_list[index].timer_num, pwm_high_cnt, pwm_low_cnt);
    }
    else
    {
        Pad_Config(led_pwm_list[index].pin_num, PAD_PINMUX_MODE, PAD_IS_PWRON, PAD_PULL_NONE,
                   PAD_OUT_DISABLE,
                   PAD_OUT_LOW);
        Pinmux_Config(led_pwm_list[index].pin_num, led_pwm_list[index].pin_func);
        RCC_PeriphClockCmd(APBPeriph_TIMER, APBPeriph_TIMER_CLOCK, ENABLE);

        TIM_TimeBaseInitTypeDef TIM_InitStruct;

        TIM_StructInit(&TIM_InitStruct);
        TIM_InitStruct.TIM_PWM_En   = PWM_ENABLE;
        TIM_InitStruct.TIM_Mode     = TIM_Mode_UserDefine;
        TIM_InitStruct.TIM_PWM_High_Count   = pwm_high_cnt;
        TIM_InitStruct.TIM_PWM_Low_Count    = pwm_low_cnt;
        TIM_InitStruct.PWM_Deazone_Size     = 0;
        TIM_InitStruct.PWMDeadZone_En       = DEADZONE_DISABLE;  //enable to use pwn p/n output
        TIM_TimeBaseInit(led_pwm_list[index].timer_num, &TIM_InitStruct);

        TIM_Cmd(led_pwm_list[index].timer_num, ENABLE);

        led_module_update_pwm_states(index, ENABLE);
    }
}

/**
 * @brief  led module pwm init.
 *@param   void.
 * @return     void.
 * @note       none.
**/
void led_module_pwm_stop(uint8_t index)
{
#if LED_PWM_DEBUG
    APP_PRINT_INFO1("[led_module_pwm_stop] index %d", index);
#endif
    TIM_Cmd(led_pwm_list[index].timer_num, DISABLE);

#if (LED_ACTIVE_LEVEL == 1)
    Pad_Config(led_pwm_list[index].pin_num, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE,
               PAD_OUT_LOW);
#else
    Pad_Config(led_pwm_list[index].pin_num, PAD_SW_MODE, PAD_IS_PWRON, PAD_PULL_NONE, PAD_OUT_ENABLE,
               PAD_OUT_HIGH);
#endif
    led_module_update_pwm_states(index, DISABLE);

    uint8_t prev_on_off_states = led_on_off_states;
    led_module_update_on_off_states(index, DISABLE);

    if ((prev_on_off_states != 0) && (led_on_off_states == 0))
    {
        led_driver_disable_boost_power();
    }
}

#endif

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
