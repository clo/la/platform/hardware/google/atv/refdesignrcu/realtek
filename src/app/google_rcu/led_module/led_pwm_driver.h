/**
*********************************************************************************************************
*               Copyright(c) 2020, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     led_pwm_driver.h
* @brief
* @details
* @author
* @date     2020-03-04
* @version  v1.0
* @note
*********************************************************************************************************
*/
#ifndef _LED_PWM_H_
#define _LED_PWM_H_

#include "board.h"
#include "swtimer.h"
#include "rtl876x_tim.h"

#if SUPPORT_LED_INDICATION_FEATURE

#define LED_PWM_DEBUG       0

#define LED_NUM_MAX         2

#define PWM_INIT_COUNT      0xFF00 //0xF82F
#define PWM_MAX_COUNT       0xFFFF

#define LED_DEFAULT_DUTY    255  // 255/255 = 100%
#define LED_DUTY_FACTOR     (0x101) //(0xFFFF/0xFF)

#define STOP_MIC_LED_BLINK_TIMEOUT 5000  /* 5 sec */

/* led pwm paramesters struct */
typedef struct
{
    uint8_t pin_num;
    uint8_t pin_func;
    TIM_TypeDef *timer_num;
} T_LED_PWM_PARAM_DEF;

/*============================================================================*
 *                        <Led Module Interface>
 *============================================================================*/
void led_pwm_init(void);
bool led_pwm_driver_dlps_check(void);
void led_module_pwm_start(uint8_t index, uint32_t pwm_high_cnt, uint32_t pwm_low_cnt);
void led_module_pwm_stop(uint8_t index);
void led_driver_enable_boost_power(void);
void led_driver_disable_boost_power(void);

#endif

#endif

/******************* (C) COPYRIGHT 2020 Realtek Semiconductor Corporation *****END OF FILE****/
