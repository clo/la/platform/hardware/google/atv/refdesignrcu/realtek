/**
*********************************************************************************************************
*               Copyright(c) 2015, Realtek Semiconductor Corporation. All rights reserved.
*********************************************************************************************************
* @file      simple_ble_peripheral_application.h
* @brief     simple_ble_peripheral_application
* @details   simple_ble_peripheral_application
* @author    jane
* @date      2015-12-22
* @version   v0.1
* *********************************************************************************************************
*/

#ifndef _PERIPHERAL_APPLICATION__
#define _PERIPHERAL_APPLICATION__

#ifdef __cplusplus
extern "C" {
#endif

/*============================================================================*
 *                        Header Files
 *============================================================================*/
#include <board.h>
#include <app_msg.h>
#include <gap_le.h>
#include <profile_server.h>
#include <gap_msg.h>
#include <test_mode.h>
#if FEATURE_SUPPORT_PRIVACY
#include "privacy_mgnt.h"
#endif

/*============================================================================*
 *                         Macros
 *============================================================================*/
/* @note:
*   keyscan debounce time should be smaller than RCU_CONNECT_INTERVAL*1.25ms, in case
*   keyscan can not be used due to keycan initialization when system enter dlps frequently,
*   you ca change keyscan debunce time in function keyscan_init_driver() and keyscan_enter_dlps_config().
*/
#define RCU_CONNECT_INTERVAL                8  /* 8 * 1.25ms = 10ms*/
#define RCU_CONNECT_LATENCY                 99
#define RCU_SUPERVISION_TIMEOUT             4000  /* 4s */

#define PPCP_CONN_INTERVAL_MIN              0x08
#define PPCP_CONN_INTERVAL_MAX              0x0B
#define PPCP_SLAVE_LATENCY                  0
#define PPCP_SUP_TIMEOUT                    1000

#define MAX_DIRECT_ADV_RETRY_CNT            3  /* max retry count for direct adv */
#define MAX_ONOFF_LATENCY_SEND_RETRY_CNT    3  /* max retry count for call le_disable_slave_latency */
#define MAX_ONOFF_LATENCY_FAILED_RETRY_CNT  5  /* max retry count for le_disable_slave_latency failure case */
#define MAX_UPDATE_CONN_PARAMS_RETRY_CNT    3  /* max retry count for connection parameters update */
#define MAX_PAIR_FAILED_RETRY_CNT           1  /* max retry count for pair failed scenario */

#define NEW_SET_LATENCY_FUNC  /* compile error workaround, todo: refactor rcu_dfu_service */

#define DEV_TYPE_REMOTE_G10                            1
#define DEV_TYPE_REMOTE_G20                            2

#define APP_CUSTOM_WAKEUPKEY_NUM 12

#define MAX_WAKE_CUSTOM_DATA_LEN   40

typedef enum
{
    ADV_IDLE = 0,
    ADV_DIRECT_HDC,
    ADV_DIRECT_LDC,
    ADV_DIRECT_LDC_LT,//long interval
    ADV_UNDIRECT_RECONNECT,
    ADV_UNDIRECT_PAIRING,
    ADV_UNDIRECT_PAIRING_LT,//long interval
    ADV_UNDIRECT_WAKEUP,
} T_ADV_TYPE;

typedef enum
{
    WAKEUP_FORMAT_GOOGLE_ONLY = 0,
    WAKEUP_FORMAT_CUSTOM_AND_GOOGLE = 1,
    WAKEUP_FORMAT_CUSTOM_ONLY = 2,
} T_WAKEUP_ADV_FORMAT;

typedef enum
{
    STOP_ADV_REASON_IDLE = 0,
    STOP_ADV_REASON_PAIRING,
    STOP_ADV_REASON_TIMEOUT,
    STOP_ADV_REASON_WAKEUPKEY,
    STOP_ADV_REASON_BACKKEY,
    STOP_ADV_REASON_LOWPOWER,
    STOP_ADV_REASON_UART_CMD,
    STOP_ADV_REASON_RESTART_RECONNECT_ADV,
} T_STOP_ADV_REASON;

typedef enum
{
    DISCONN_REASON_IDLE = 0,
    DISCONN_REASON_PAIRING,
    DISCONN_REASON_TIMEOUT,
    DISCONN_REASON_OTA,
    DISCONN_REASON_PAIR_FAILED,
    DISCONN_REASON_LOW_POWER,
    DISCONN_REASON_UART_CMD,
    DISCONN_REASON_SILENT_OTA,
} T_DISCONN_REASON;

typedef enum
{
    RCU_STATUS_IDLE = 0,  /* RCU idle status */
    RCU_STATUS_ADVERTISING,  /* RCU adversting status */
    RCU_STATUS_STOP_ADVERTISING,  /* temporary status of stop adversting */
    RCU_STATUS_CONNECTED,  /* connect status but not start paring */
    RCU_STATUS_PAIRED,  /* rcu paired success status */
    RCU_STATUS_DISCONNECTING,  /* temporary status of disconnecting */
    RCU_STATUS_LOW_POWER,     /*rcu low power mode*/
} T_RCU_STATUS;

/**
 * @brief  APP latency state enum definition.
 */
typedef enum
{
    LANTENCY_ON = 0,
    LANTENCY_OFF,
} T_LATENCY_STATE;

typedef enum
{
    LATENCY_SYS_UPDATE_BIT    = 0,
    LATENCY_PAIR_PROC_BIT     = 1,
    LATENCY_VOICE_PROC_BIT    = 2,
    LATENCY_DFU_PROC_BIT      = 3,
    LATENCY_IR_BLE_PROC_BIT   = 4,
    LATENCY_TEMP_OFF_BIT   = 5,
    LATENCY_MAX_BIT_NUM,
} T_APP_LATENCY_MASK;

/**
 * @brief  APP latency status struct definition.
 */
typedef struct
{
    bool is_updating;
    bool is_pending;
    uint8_t retry_cnt;
    T_APP_LATENCY_MASK cur_index_bit;
    T_APP_LATENCY_MASK pending_index_bit;
    T_LATENCY_STATE cur_status;
    T_LATENCY_STATE pending_status;
    uint16_t latency_bits_map;
} T_latency_status;

/**
 * @brief  APP battery status status definition.
 */
typedef struct
{
    uint16_t voltage_value;
    uint16_t level_value;
} T_BATTERY_STATUS;



/**
 * @brief  APP global data struct definition.
 */
typedef struct
{
    bool is_link_key_existed;  /* to indicate whether link key is existed or not */
    bool is_keyboard_notify_en;  /* to indicate whether keyboard notification is enabled or not */
    bool is_atv_voice_notify_en; /* to indicate whether voice(attv) notification is enabled or not */
    bool is_mm_keyboard_notify_en;  /* to indicate whether multimediea keyboard notification is enabled or not */
    uint8_t device_type;  /* to indicate the device type */
    uint8_t device_ui_layout_type;  /* to indicate the device ui layout type */
    uint8_t en_find_me;  /* to indicate whether Find Me is enabled or not */
    uint8_t en_rpa;  /* to indicate whether RPA is enabled or not */
    uint8_t en_ble_feature;   /* to indicate whether BLE Feature is enabled or not */
    uint8_t en_powerkey_cache;  /* to indicate whether power key cache is enabled or not */
    uint8_t en_google_wakeuppack;  /* to indicate whether google wakeuppack enabled or not */
    uint8_t direct_adv_cnt;  /* to indicate the count of high duty adv */
    uint8_t updt_conn_params_retry_cnt;  /* to indicate the retry count for connection parameter update */
    uint8_t mtu_size;  /* to indicate the current mtu size */
    uint8_t pair_failed_retry_cnt;  /* to indicate the retry cnt for pair failed scenario */
    uint16_t find_me_wakeup_timeout;  /* to indicate the find me wakeup timeout */
    uint8_t wakeup_key_index;  /* to indicate the wakeup key index */
    uint32_t wakeup_key_count;  /* to indicate the wakeup key count */
    T_WAKEUP_ADV_FORMAT wakeup_adv_format;  /* to indicate the wakeup adv format */
    T_SW_RESET_REASON reset_reason;  /* to indicate the reset reason */
    uint8_t wakeup_adv_custom_data[MAX_WAKE_CUSTOM_DATA_LEN];  /* to indicate the data buffer of customized wakeup adv */
    T_SERVER_ID bas_srv_id;  /* to indicate the service id for BAS */
    T_SERVER_ID dis_srv_id;  /* to indicate the service id for DIS */
    T_SERVER_ID hid_srv_id;  /* to indicate the service id for HID */
    T_SERVER_ID ir_srv_id;   /* to indicate the service id for IR */
    T_SERVER_ID vendor_srv_id;  /* to indicate the service id for vendor service */
    T_SERVER_ID ota_srv_id;  /* to indicate the service id for OTA service */
    T_SERVER_ID dfu_srv_id;  /* to indicate the service id for DFU service */
    T_SERVER_ID atvv_srv_id;  /* to indicate the service id for ATVV service */
    T_SERVER_ID voice_srv_id;  /* to indicate the service id for voice service */
    T_SERVER_ID fms_srv_id;  /* to indicate the service id for FMS service */
    T_RCU_STATUS rcu_status;  /* to indicate the current status of rcu state machine */
    T_GAP_DEV_STATE gap_dev_state;  /* to indicate the current GAP device state */
    T_GAP_CONN_STATE gap_conn_state;  /* to indicate the current GAP connection state */
    T_ADV_TYPE adv_type;  /* to indicate the current advertising type */
    T_STOP_ADV_REASON stop_adv_reason;  /* to indicate the reason of stop advertising */
    T_DISCONN_REASON disconn_reason;  /* to indicate the reason of disconnec  */
    T_BATTERY_STATUS battery_status;  /* to indicate the status of battery information */
    T_latency_status latency_status;  /* to indicate rcu app latency status */
    T_TEST_MODE test_mode;  /* to indicate current test mode */
    uint16_t conn_interval;  /* to indicate rcu connect interval */
    uint16_t conn_latency;   /* to indicate rcu connect latency */
    uint16_t conn_supervision_timeout; /* to indicate rcu supervision time out */
    uint16_t wakeupkey1_mask;  /* to indicate the wakeup key1 mask */
    uint16_t wakeupkey2_mask;  /* to indicate the wakeup key2 mask */
    uint8_t wakeup_key1[APP_CUSTOM_WAKEUPKEY_NUM];  /* to indicate the key ids with wakeup */
    uint8_t wakeup_key2[APP_CUSTOM_WAKEUPKEY_NUM];  /* to indicate the key ids with wakeup */
    uint8_t pnp_id[7];  /* to indicate the info of PnP Id  */
    uint8_t fw_version[6];  /* to indicate the info of FW version */
    uint8_t sw_version[5];  /* to indicate the info of SW version */
    uint8_t device_name_len;  /* to indicate the length of device name */
    uint8_t device_name[20];  /* to indicate the info of device name */
    uint8_t is_allow_to_factory_reset_for_back_key;  /* chaeck if rcu can reset by back key */
    bool allow_to_send_two_ble_key_value;            /*check if allow to send two ble key value */
} T_APP_GLOBAL_DATA;

extern T_APP_GLOBAL_DATA app_global_data;
#if FEATURE_SUPPORT_PRIVACY
extern T_PRIVACY_STATE app_privacy_state;
extern T_PRIVACY_ADDR_RESOLUTION_STATE app_privacy_resolution_state;
#endif

#if !FEATURE_SUPPORT_PRIVACY
void app_handle_bond_modify_msg(T_LE_BOND_MODIFY_TYPE type, T_LE_KEY_ENTRY *p_entry);
#endif
T_APP_RESULT app_profile_callback(T_SERVER_ID service_id, void *p_data);
T_APP_RESULT app_gap_callback(uint8_t cb_type, void *p_cb_data);
void app_handle_io_msg(T_IO_MSG io_driver_msg_recv);
void app_set_latency_status(T_APP_LATENCY_MASK index_bit, T_LATENCY_STATE new_state);
void app_init_global_data(void);
void app_nvic_config(void);
#if FEATURE_SUPPORT_PRIVACY
void app_privacy_callback(T_PRIVACY_CB_TYPE type, T_PRIVACY_CB_DATA cb_data);
#endif
bool app_load_wakeup_key_cnt(uint32_t *p_count);
bool app_save_wakeup_key_cnt(uint32_t save_count);

#ifdef __cplusplus
}
#endif

#endif

