/**
*********************************************************************************************************
*               Copyright(c) 2022, Realtek Semiconductor Corporation. All rights reserved.
*********************************************************************************************************
* @file      frm_define.h
* @brief     header file of framework defines.
* @details
* @author    chenjie_jin
* @date      2022-03-09
* @version   v1.0
* *********************************************************************************************************
*/


#ifndef _FRM_DEFINE_H_
#define _FRM_DEFINE_H_

#include "otp_config.h"
#include "flash_map.h"

#ifdef __cplusplus
extern "C" {
#endif

#define _attribute_data_retention_

/* exact-width signed integer types */
typedef   signed          char s8;
typedef   signed short     int s16;
typedef   signed           int s32;

/* exact-width unsigned integer types */
typedef unsigned          char u8;
typedef unsigned short     int u16;
typedef unsigned           int u32;


#define U16_HI(a)    (((a) >> 8) & 0xFF)
#define U16_LO(a)    ((a) & 0xFF)

#define FLASH_SECTOR_SIZE         (4*1024)
#define FLASH_BLOCK_SIZE          (64*1024)

#define APP_CUSTOM_ADDR                       (BKP_DATA1_ADDR + FLASH_SECTOR_SIZE * 0)
#define APP_FMS_WAKEUP_INTERVAL               (APP_CUSTOM_ADDR + 0xF9)
#define APP_EN_FIND_ME                        (APP_CUSTOM_ADDR + 0xFB)
#define APP_EN_RPA                            (APP_CUSTOM_ADDR + 0xFC)
#define APP_EN_BLE_FEATURE                    (APP_CUSTOM_ADDR + 0xFD)
#define APP_EN_GOOGLE_WAKEUPPACK              (APP_CUSTOM_ADDR + 0xFE)
#define APP_EN_CACHEKEY                       (APP_CUSTOM_ADDR + 0xFF)
#define APP_NEC_IR_CODE_TABLE                 (BKP_DATA1_ADDR + FLASH_SECTOR_SIZE * 1)

#ifdef __cplusplus
}
#endif

#endif  /* _FRM_DEFINE_H_ */

