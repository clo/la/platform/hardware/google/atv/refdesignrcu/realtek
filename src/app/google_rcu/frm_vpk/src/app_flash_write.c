/******************************************************************************
 * @file     app_info.c
 *
 * @brief    for TLSR chips
 *
 * @author   public@telink-semi.com;
 * @date     Sep. 30, 2010
 *
 * @attention
 *
 *  Copyright (C) 2019-2020 Telink Semiconductor (Shanghai) Co., Ltd.
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 *
 *****************************************************************************/

#include "string.h"
#include "app_flash_write.h"
#include "frm_vpk_hal_sflash.h"


_attribute_data_retention_ u8 flag_ccc_data = 0;
_attribute_data_retention_ u8 peer_mac[6] = {0};



/**********************************************************
 **** operate  flash function
 **********************************************************/

/**
 * @brief This function can make more effective use of flash.
 * @param[in]   addr the start address of the page
 * @param[in]   Number of sectors used to save this data type
 * @param[in]   Space occupied by a piece of data.Less than 256��
 * @return -1:No data found,others:address offset
 */
s16 bsearch_without_recursion(unsigned long addr, u8 used_sector, u8 used_block)
{
    s16 low = 0, high = (4 * 1024 / used_block) * used_sector - 1;
    u8 rbuf[2];
    u8 cmp1[] = {U16_LO(FLASH_FLAG), U16_HI(FLASH_FLAG)}; //{0xa5,0xff};
    u8 cmp2[] = {0x00, 0x00};
    u8 cmp3[] = {0xff, 0xff};
    while (low <= high)
    {
        //DEBUG("low: %d  high: %d\n",low,high);
        s16 mid = (low + high) / 2;

        frm_vpk_flash_read_page(addr + mid * used_block, 2, rbuf);
        //frm_vpk_print("bsearchWithoutRecursion tmp:");
        //arrayPrint(tmp,2);
        //frm_vpk_print("\n:");
        if (!memcmp(rbuf, cmp3, 2)) { high = mid - 1; }
        else if (!memcmp(rbuf, cmp2, 2)) { low = mid + 1; }
        else if (!memcmp(rbuf, cmp1, 2)) { return mid; } //return mid*used_block;
        else { return -1; }
    }
    return -1;
}

/**
 * @brief This function can make more effective use of flash.
 * @param[in]   addr the start address of the page
 * @param[in]   Number of sectors used to save this data type
 * @param[in]   Space occupied by a piece of data.Less than 256��
 * @param[in]   len the length(in byte) of content needs to read out from the page
 * @param[out]  buf the start address of the buffer
 * @return 0:Success,1:flash is empty,2:sector data error
 */
u8 flash_pos_info(unsigned long addr, u8 used_sector, u8 used_block)
{
    u8 ret = 0;
    u8 begin_data[2];    //beginning of sector data.byte 0,1 is 1st sector data,byte 2,3 is 2nd sector data.

    s16 high = (4 * 1024 / used_block) * used_sector - 1;

    //Read the beginning and end data of the 1st sector
    frm_vpk_flash_read_page(addr, 2, begin_data);
    //flash_read_page(addr, 2, end_data);

    u8 allff[2] = {0xff, 0xff};

    /****  start check flash  ****/

    if (!memcmp(begin_data, allff, 2))
    {
        //beginning of 1st sector data(2byte) is all 0xff.
        //Indicates whether the RCU is brand new or factory set
        ret = 1;    //return 1;    //No data in flash
    }
    else
    {

        //search data in flash
        s16 offset = bsearch_without_recursion(addr, used_sector, used_block);
        //frm_vpk_print("offset=%x \r\n", (u16)offset);
        if (offset == -1)
        {

            ret = 2;    //return 2;    //sector data error
        }
        else
        {
            if (offset > (high / 10 * 9))
            {
                return 3;
            }
        }
    }

    return ret;
}


/**
 * @brief This function can make more effective use of flash.
 * @param[in]   addr the start address of the page
 * @param[in]   Number of sectors used to save this data type
 * @param[in]   Space occupied by a piece of data.Less than 256��
 * @param[in]   len the length(in byte) of content needs to read out from the page
 * @param[out]  buf the start address of the buffer
 * @return 0:Success,1:flash is empty,2:sector data error
 */
u8 flash_read_info(unsigned long addr, u8 used_sector, u8 used_block, unsigned long len, u8 *rbuf)
{
    u8 ret = 0;
    u8 begin_data[2];    //beginning of sector data.byte 0,1 is 1st sector data,byte 2,3 is 2nd sector data.
    //Read the beginning and end data of the 1st sector
    frm_vpk_flash_read_page(addr, 2, begin_data);
    //flash_read_page(addr, 2, end_data);

    u8 allff[2] = {0xff, 0xff};

    /****  start check flash  ****/

    if (!memcmp(begin_data, allff, 2))
    {
        //beginning of 1st sector data(2byte) is all 0xff.
        //Indicates whether the RCU is brand new or factory set
        ret = 1;    //return 1;    //No data in flash
    }
    else
    {

        //search data in flash
        s16 offset = bsearch_without_recursion(addr, used_sector, used_block);

        if (offset == -1)
        {

            ret = 2;    //return 2;    //sector data error
        }
        else
        {

            frm_vpk_flash_read_page(addr + offset * used_block, len, rbuf);
        }
    }

    return ret;
}

/**
 * @brief This function can make more effective use of flash.
 * @param[in]   addr the start address of the page
 * @param[in]   Number of sectors used to save this data type
 * @param[in]   Space occupied by a piece of data.Less than 256��
 * @param[in]   len the length(in byte) of content needs to read out from the page
 * @param[out]  buf the start address of the buffer
 * @return 0:Success,1:flash is empty,2:sector data error,3:read data error
 */
u8 flash_write_info(unsigned long addr, u8 used_sector, u8 used_block, unsigned long len, u8 *wbuf)
{

    u8 ret = 0;
    u8 begin_data[2];    //beginning of sector data.byte 0,1 is 1st sector data,byte 2,3 is 2nd sector data.
    //u8 end_data[4];        //end of sector data.byte 0,1 is 1st sector data,byte 2,3 is 2nd sector data.

    //Read the beginning and end data of the 1st sector
    frm_vpk_flash_read_page(addr, 2, begin_data);
    //flash_read_page(addr, 2, end_data);

    u8 allff[2] = {0xff, 0xff};

    /****  start check flash  ****/

    if (!memcmp(begin_data, allff, 2))
    {
        //beginning of 1st sector data(2byte) is all 0xff.
        //Indicates whether the RCU is brand new or factory set
        ret = 1;    //No data in flash
    }
    else
    {

        //search data in flash
        s16 offset = bsearch_without_recursion(addr, used_sector, used_block);

        if (offset == -1)
        {

            ret = 2;    //sector data error
        }
        else
        {
            u8 clr[2] = {0};
            frm_vpk_flash_write_page(addr + offset * used_block, 2, clr);

            //if(offset + 1 >= 4*1024*used_sector/used_block) offtset = -1;    //2nd sector is full.so set offset to beginning of 1st sector
            //write data to flash
            frm_vpk_flash_write_page(addr + (offset + 1)*used_block, len, wbuf);
            //Confirm that writing data is correct
            //u8 temp[256] = {0};
            u8 temp[16] = {0};
            frm_vpk_flash_read_page(addr + (offset + 1)*used_block, len, temp);
            if (memcmp(wbuf, temp, len)) { ret = 3; }  //The data written and read does not match
        }
    }

    switch (ret)
    {
    case 3:
    case 2:
        break;
    case 1:
        {
            frm_vpk_flash_write_page(addr, len, wbuf);
            break;
        }
    case 0:
    default:
        break;
    }

    return ret;
}

void flash_check_area(unsigned long addr, u8 used_sector, u8 used_block, unsigned long len)
{


    u8 begin_data[2];    //beginning of sector data.byte 0,1 is 1st sector data,byte 2,3 is 2nd sector data.
    u8 allff[2] = {0xff, 0xff};
    //Read the beginning and end data of the 1st sector
    frm_vpk_flash_read_page(addr, 2, begin_data);

    if (!memcmp(begin_data, allff, 2))
    {
        //beginning of 1st sector data(2byte) is all 0xff.
        //Indicates whether the RCU is brand new or factory set
        return;
    }


    s16 offset = bsearch_without_recursion(addr, used_sector, used_block);
    if (offset == -1)
    {
        //sector data error
        for (u8 i = 0; i < used_sector; i++)
        {
            frm_vpk_flash_erase_sector(addr + i * 0x1000);
        }
        return;
    }
}

