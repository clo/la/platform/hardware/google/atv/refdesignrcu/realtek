/**
*********************************************************************************************************
*               Copyright(c) 2021, Realtek Semiconductor Corporation. All rights reserved.
**********************************************************************************************************
* @file     frm_vpk_log.c
* @brief    frm vpk log API implementation
* @details
* @author   chenjie
* @date     2021-01-20
* @version  v0.1
*********************************************************************************************************
*/

/*============================================================================*
 *                              Header Files
 *============================================================================*/
#include <stdio.h>
#include <stdarg.h>
#include "frm_vpk_log.h"
#include "trace.h"
#include "app_section.h"


/*============================================================================*
 *                              Macro Definitions
 *============================================================================*/
#define MAX_LOG_MESSAGE_LEN           120

/*============================================================================*
 *                              Functions Declaration
 *============================================================================*/
int frm_vpk_print(char *fmt, ...) DATA_RAM_FUNCTION;

/*============================================================================*
 *                              Local Variables
 *============================================================================*/

/*============================================================================*
 *                              Global functions
 *============================================================================*/
/**
 * @brief API implements the console log printing.
 *
 */
int frm_vpk_print(char *fmt, ...)
{
#if 0
    APP_PRINT_INFO1("[frm_vpk_print] fmt is %s", TRACE_STRING(fmt));
#endif

    int ret_value;
    char print_str[MAX_LOG_MESSAGE_LEN];
    va_list args;
    va_start(args, fmt);

    ret_value = vsprintf(print_str, (const char *)fmt, args);
    APP_PRINT_INFO1("%s", TRACE_STRING(print_str));

    va_end(args);

    return ret_value;
}

/******************* (C) COPYRIGHT 2021 Realtek Semiconductor Corporation *****END OF FILE****/

